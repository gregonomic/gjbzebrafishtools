#!/usr/bin/env python

from __future__ import print_function

from collections import Counter

# MARK: HomozygosityWindow

class HomozygosityWindow(object):
    """HomozygosityWindow factory, returns appropriate window type based on
    'nsnps' argument
    
    Arguments:
        as for HomozygosityWindowBase
        
    Keyword Arguments:
        nsnps (bool): True for HomozygosityWindowNsnps, False for
            HomozygosityWindowNbps; default=False
        otherwise as for HomozygosityWindowBase
    
    Returns:
        homozygosity window (HomozygosityWindowNsnps or HomozygosityWindowNbps)
    """
    def factory(*args, **kwargs):
        nsnps = kwargs.pop('nsnps', False)
        if nsnps:
            return HomozygosityWindowNsnps(*args)
        else:
            return HomozygosityWindowNbps(*args)

    factory = staticmethod(factory)

class HomozygosityWindowBase(object):
    """Base HomozygosityWindow class"""
    
    def __init__(self, chromosome, start, segment, window_size, step_size):
        """Arguments:
            chromosome (str)        : name of chromosome/contig
            start (int)             : start position of window
            segment (ContigSegment) : a ContigSegment nametuple for segment in
                which window is located
            window_size (int)       : size of window, in base pairs or number
                of informative positions (SNPs)
            step_size (int)         : step size, in base pairs or number of
                informative positions (SNPs)

        Attributes:
            chromosome (str)        : name of chromosome/contig
            start (int)             : start position of window
            segment (ContigSegment) : a ContigSegment nametuple for segment in
                which window is located
            window_size (int)       : size of window, in base pairs or number
                of informative positions (SNPs)
            step_size (int)         : step size, in base pairs or number of
                informative positions (SNPs)
            counts (Counter)        : counter for keeping track of counts in
                window
            score (float)           : score for window
        """
        self.chromosome = chromosome
        self.start = start
        self.end = None
        self.segment = segment
        self.window_size = window_size
        self.step_size = step_size
        self.counts = Counter()
        self.score = None

    def bedgraph_start(self, prev_window=None):
        """Calculate the start for bedgraph output
        
        Keyword Arguments:
            prev_window (HomozygosityWindowNsnps): previous window in same
                chromosome (default=None)
            
        Returns:
            int: bedgraph start
        """
        raise NotImplementedError("bedgraph_start() not implemented")

    def bedgraph_end(self, next_window=None):
        raise NotImplementedError("bedgraph_end() not implemented")

    def bedgraph_string(self, prev_window, next_window):
        raise NotImplementedError("bedgraph_string() not implemented")

class HomozygosityWindowNbps(HomozygosityWindowBase):
    """HomozygosityWindow with window size and step size defined by number of
    base-pairs
    """
    
    def __init__(self, *args):
        super(HomozygosityWindowNbps, self).__init__(*args)
        if self is not None:
            self._calculate_end()

    def _calculate_end(self):
        self.end = self.start + self.window_size - 1
        if self.end > self.segment.end:
            self.end = self.segment.end

    def bedgraph_start(self, prev_window=None):
        if prev_window is None:
            # This is the first window in contig/chrom
            return self.segment.start
        elif prev_window.segment.start != self.segment.start:
            # This is the first window in segment
            return self.segment.start
        else:
            # Windows are from same segment
            return int(
                self.start
                + (self.window_size / 2)
                - (self.step_size / 2)
            )

    def bedgraph_end(self, next_window=None):
        """Calculate the end for bedgraph output
        
        Keyword Arguments:
            next_window (HomozygosityWindowNsnps): next window in same
                chromosome (default=None)
            
        Returns:
            int: bedgraph end
        """
        if next_window is None:
            # This is the last window in contig/chrom
            return self.segment.end
        elif self.segment.end != next_window.segment.end:
            # This is the last window in segment
            return self.segment.end
        else:
            # Windows are from the same segment
            return int(
                self.end
                - (self.window_size / 2)
                + (self.step_size / 2)
            )

    def bedgraph_string(self, prev_window, next_window):
        """Create bedgraph string
        
        It is assumed that score has already been set
        
        Keyword Arguments:
            prev_window (HomozygosityWindowNsnps): previous window in same
                chromosome (default=None)
            next_window (HomozygosityWindowNsnps): next window in same
                chromosome (default=None)
        
        Returns:
            str: tab-separated bedgraph string in format: chromosome start end
                value
        """
        return "{0}\t{1}\t{2}\t{3:.2f}".format(
            self.chromosome,
            self.bedgraph_start(prev_window=prev_window),
            self.bedgraph_end(next_window=next_window),
            self.score
        )

    def __unicode__(self):
        counts_str = ",".join(
            "{0}:{1}".format(key, self.counts[key])
             for key in sorted(self.counts.keys)
        )
        return "{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}".format(
            self.chromosome,
            self.start,
            self.end,
            self.segment.start,
            self.segment.end,
            self.window_size,
            self.step_size,
            counts_str,
            self.score
        )

class HomozygosityWindowNsnps(HomozygosityWindowBase):
    """HomozygosityWindow with window size and step size defined by number of
    SNPs
    """

    def __init__(self, *args):
        super(HomozygosityWindowNsnps, self).__init__(*args)
        self.number_of_informative_positions = 0
        self.bedgraph_start_pos = None
        self.bedgraph_end_pos = None
        self.bedgraph_start_number_of_positions = (
            self._calculate_bedgraph_start_number_of_positions())
        self.bedgraph_end_number_of_positions = (
            self._calculate_bedgraph_end_number_of_positions())

    def _calculate_bedgraph_start_number_of_positions(self):
        """Calculate the number of informative positions that define the
        bedgraph start
        """
        if self.window_size == self.step_size:
            # bedgraph start will be first informative position in window
            return 1
        if (self.window_size % 2) != 0:
            # Window size is odd number of SNPs, middle position will fall on
            # middle position
            # Step size must be odd
            window_position_mid_index = (self.window_size + 1) / 2
            return window_position_mid_index - ((self.step_size - 1) / 2)
        else:
            # Window size is even number of SNPs, middle position will be half
            # way between two middle positions of window
            window_position_mid_l_index = (self.window_size / 2) - 1
            if (self.step_size % 2) != 0:
                # Step size is odd
                return window_position_mid_l_index - ((self.step_size + 1) / 2)
            else:
                # Step size is even
                return window_position_mid_l_index - (self.step_size / 2)

    def _calculate_bedgraph_end_number_of_positions(self):
        """Calculate the number of informative positions that define the
        bedgraph end
        """
        if (self.window_size == self.step_size):
            # bedgraph end will be last informative position in window
            return self.window_size
        if (self.window_size % 2) != 0:
            # Window size is odd number of SNPs, middle position will fall on
            # middle position
            # Step size must be odd
            window_position_mid_index = (self.window_size + 1) / 2
            return window_position_mid_index + ((self.step_size - 1) / 2)
        else:
            # Window size is even number of SNPs, middle position will be half
            # way between two middle positions of window
            if (self.step_size % 2) != 0:
                # Step size is odd
                window_position_mid_l_index = (self.window_size / 2) - 1
                return window_position_mid_l_index + ((self.step_size + 1) / 2)
            else:
                # Step size is even
                window_position_mid_r_index = (self.window_size / 2) + 1
                return window_position_mid_r_index + (self.step_size / 2)

    def bedgraph_start(self, prev_window=None):
        """Calculate the start for bedgraph output
        
        Keyword Arguments:
            prev_window (HomozygosityWindowNsnps): previous window in same
                chromosome (default=None)
            
        Returns:
            int: bedgraph start
        """
        if prev_window is None:
            # This is the first window in contig/chrom
            return self.segment.start
        elif prev_window.segment.start != self.segment.start:
            # This is the first window in segment
            return self.segment.start
        else:
            this_end = self.end if self.end is not None else self.segment.end
            prev_end = (prev_window.end
                if prev_window.end is not None
                else prev_window.segment.end)
            this_w_start = (self.bedgraph_start_pos
                if self.bedgraph_start_pos is not None
                else self.segment.start)
            prev_w_end = (prev_window.bedgraph_end_pos
                if prev_window.bedgraph_end_pos is not None
                else prev_window.segment.end)
            if (this_end == prev_end):
                # This window has the same end position as the previous window;
                # this window should not be written, so bedgraph_start is
                # irrelevant
                return None
            elif (this_w_start == prev_w_end):
                return this_w_start + 1
            else:
                return ((this_w_start + prev_w_end) / 2) + 1

    def bedgraph_end(self, next_window=None):
        """Calculate the end for bedgraph output
        
        Keyword Arguments:
            next_window (HomozygosityWindowNsnps): next window in same
                chromosome (default=None)
            
        Returns:
            int: bedgraph end
        """
        if next_window is None:
            # This is the last window in contig/chrom
            return self.segment.end
        elif self.segment.end != next_window.segment.end:
            # This is the last window in segment
            return self.segment.end
        else:
            this_w_end = (self.bedgraph_end_pos
                if self.bedgraph_end_pos is not None
                else self.segment.end)
            next_w_start = (next_window.bedgraph_start_pos
                if next_window.bedgraph_start_pos is not None
                else next_window.segment.start)
            if self.number_of_informative_positions < self.window_size:
                return self.segment.end
            elif this_w_end == next_w_start:
                return this_w_end
            else:
                return (this_w_end + next_w_start) / 2

    def bedgraph_string(self, prev_window, next_window):
        """Create bedgraph string
        
        It is assumed that score has already been set
        
        Arguments:
            prev_window (HomozygosityWindowNsnps): previous window in same
                chromosome (default=None)
            next_window (HomozygosityWindowNsnps): next window in same
                chromosome (default=None)
        
        Returns:
            str: tab-separated bedgraph string in format: chromosome start end
                value
        """
        w_start = self.bedgraph_start(prev_window=prev_window)
        w_end = self.bedgraph_end(next_window=next_window)
        if self.score is not None and w_start is not None and w_end is not None:
            return "{0}\t{1}\t{2}\t{3:.2f}".format(
                self.chromosome,
                w_start,
                w_end,
                self.score
            )
        else:
            return None

# MARK: HomozygosityWindowController
            
class HomozygosityWindowController(object):
    """Factory for returning HomozygosityWindowControllerNbps or
    HomozygosityWindowControllerNsnps window
    """
    
    def factory(nsnps, **kwargs):
        if nsnps:
            return HomozygosityWindowControllerNsnps(**kwargs)
        else:
            return HomozygosityWindowControllerNbps(**kwargs)

    factory = staticmethod(factory)

class HomozygosityWindowControllerBase(object):
    """Base HomozygosityWindowController class"""
    
    def __init__(self, window_size=None, step_size=None, contig_coords=None,
        scorer=None):
        self.contig_coords = contig_coords # a VCFContigCoordinates instance
        self.scorer = scorer # A HomozygosityScorer instance
        self.window_size = window_size
        self.step_size = step_size
        self.windows = []
        self.start_index = 0
        self.current_contig = None # A VCFContig instance
        self.current_contig_segments = None
        self.current_segment = None
        self.current_segment_index = 0

    def prepare_for_new_contig_with_record(self, record):
        """Prepare for new contig, ie. clear windows and reset start_index, get
         new contig and segment coords
        
        Arguments:
            record (VCFRecord): a VCFRecord
        """
        self.windows = []
        self.start_index = 0
        self.current_contig = (
            self.contig_coords.get_contig_with_name(record.CHROM))
        self.current_contig_segments = self.current_contig.gapped_coords()
        self.current_segment = self.current_contig_segments[0]
        self.current_segment_index = 0

    def set_scores(self):
        """Set scores in all windows"""
        for window in self.windows:
            window.score = self.scorer.score_from_counts(window.counts)

    def update_count_in_windows_for_key(self, pos, key, informative_key=None):
        """Abstract method to be implemented in subclasses"""
        raise NotImplementedError(
            "update_count_in_windows_for_key() not implemented")

class HomozygosityWindowControllerNbps(HomozygosityWindowControllerBase):
    """HomozygosityWindowController that handles HomozygosityWindowNbps
    windows
    """
    
    def __init__(self, **kwargs):
        window_size = kwargs.pop('window_size', 10000)
        step_size = kwargs.pop('step_size', 1000)
        super(HomozygosityWindowControllerNbps, self).__init__(
                window_size=window_size,
                step_size=step_size,
                **kwargs)
    
    def process_vcf_record(self, record):
        """Process VCFRecord, and return windows to write if this is a new
            contig
        
        Arguments:
            record (VCFRecord): a VCFRecord
        
        Returns:
            list of windows to write if this is a new contig, else None
        """
        windows_to_write = None
        if self.current_contig is None:
            self.prepare_for_new_contig_with_record(record)
        elif record.CHROM != self.current_contig.name:
            self.set_scores()
            windows_to_write = self.windows
            # Prepare for new contig
            self.prepare_for_new_contig_with_record(record)
            
        # Determine which counts to increment
        key_to_inc = self.scorer.key_to_increment(record)
        
        # Now increment counts for key
        if key_to_inc > 0:
            self.update_count_in_windows_for_key(record.POS, key_to_inc)

        return windows_to_write

    def update_count_in_windows_for_key(self, pos, key, informative_key=None):
        """Update counts for 'key' in windows that surround 'pos'
        
        Arguments:
            pos (int)                      : position in chromosome
            key (int, str, ...)            : key to update in window counts
            informative_key (int, str, ...): scorer informative key
                (not used for nbps windows)
        """
        for window in self.windows[self.start_index:]:
            if window.end < pos:
                # pos is past the current start index
                self.start_index += 1
                continue
            elif window.start > pos:
                # This window is beyond pos, stop
                break
            else:
                # pos is in this window
                window.counts[key] += 1

    def prepare_for_new_contig_with_record(self, record):
        """Extends parent class prepare_for_new_contig_with_record(), by
        creating all windows for new contig
        """
        super(HomozygosityWindowControllerNbps,
            self).prepare_for_new_contig_with_record(record)
        self.create_windows_for_contig(self.current_contig)

    def create_windows_for_contig(self, contig):
        """Receives a contig, and creates suitable windows

        Keyword Arguments:
            contig (VCFContig): a VCFContig
        """
        # Make windows
        segment_coords = contig.gapped_coords()
        for segment in segment_coords:
            # Make and add the windows for this segment
            for window_start in range(
                segment.start,
                segment.end,
                self.step_size
            ):
                # Make the window
                window = HomozygosityWindow.factory(
                    contig.name,
                    window_start,
                    segment,
                    self.window_size,
                    self.step_size,
                    nsnps=False
                )
                self.windows.append(window)
                if window_start > (segment.end - self.window_size):
                    break

class HomozygosityWindowControllerNsnps(HomozygosityWindowControllerBase):

    def __init__(self, **kwargs):
        window_size = kwargs.pop('window_size', 200)
        step_size = kwargs.pop('step_size', 20)
        super(HomozygosityWindowControllerNsnps, self).__init__(
            window_size=window_size,
            step_size=step_size,
            **kwargs)

    def process_vcf_record(self, record):
        """Process VCFRecord, and return windows to write if this is a new
        contig.
        
        Arguments:
            record (VCFRecord): a VCFRecord
        
        Returns:
            list of windows to write if this is a new contig, else None
        """
        windows_to_write = None
        if self.current_contig is None:
            # Update current_contig
            self.prepare_for_new_contig_with_record(record)
        elif record.CHROM != self.current_contig.name:
            self.set_scores()
            windows_to_write = self.windows
            # Update current_contig
            self.prepare_for_new_contig_with_record(record)

        # Check if we've entered a new segment, ie. we've moved over a gap
        if record.POS > self.current_segment.end:
            # We have entered a new segment, move to new segment, making empty
            # windows as required
            self.add_missing_windows(record)
            # Set start index to last window
            self.start_index = len(self.windows) - 1
        
        # Check whether we need to start a new step, ie. end window informative
        # positions count == step size
        end_window = self.windows[-1] #if len(self.windows) > 0 else None
        if end_window.number_of_informative_positions == self.step_size:
            new_window = HomozygosityWindow.factory(
                record.CHROM,
                record.POS,
                self.current_segment,
                self.window_size,
                self.step_size,
                nsnps=True
            )
            self.windows.append(new_window)

        # Check whether window at start_index has window_size
        # informative_positions_count
        if (self.windows[self.start_index].number_of_informative_positions ==
                self.window_size):
            # It does; increment start_index
            self.start_index += 1

        # Update end of all relevant windows
        for window in self.windows[self.start_index:]:
            window.end = record.POS

        # Determine which counts to increment
        key_to_inc = self.scorer.key_to_increment(record)
        
        # Now increment counts for key
        if key_to_inc > 0:
            self.update_count_in_windows_for_key(
                record.POS,
                key_to_inc,
                informative_key=self.scorer.informative_key
            )

        return windows_to_write

    def update_count_in_windows_for_key(self, pos, key, informative_key=None):
        """Update counts for 'key' in windows that surround 'pos'
        
        Arguments:
        pos (int):       position in chromosome
        key:             key to update in window counts
        
        Keyword Arguments:
        informative_key: scorer informative key (not used for nbps windows)
        """
        for window in self.windows[self.start_index:]:
            if window.end < pos:
                # pos is past the current start index
                self.start_index += 1
                continue
            elif window.start > pos:
                # This window is beyond pos, stop
                break
            else:
                # pos is in this window
                window.counts[key] += 1
                if informative_key & key:
                    window.number_of_informative_positions += 1
                    
                    if (window.number_of_informative_positions ==
                            window.bedgraph_start_number_of_positions):
                        window.bedgraph_start_pos = pos
                        
                    if (window.number_of_informative_positions ==
                            window.bedgraph_end_number_of_positions):
                        window.bedgraph_end_pos = pos

    def prepare_for_new_contig_with_record(self, record):
        """Extends parent class prepare_for_new_contig_with_record(), by
        creating first window and missing windows"""
        super(HomozygosityWindowControllerNsnps,
            self).prepare_for_new_contig_with_record(record)
        pos = record.POS if record.POS < self.current_segment.end else None
        new_window = HomozygosityWindow.factory(
            self.current_contig.name,
            pos,
            self.current_segment,
            self.window_size,
            self.step_size,
            nsnps=True
        )
        self.windows.append(new_window)
        if record.POS > self.current_segment.end:
            self.add_missing_windows(record)

    def add_missing_windows(self, record):
        """Add windows that are missing between last window and current position
        
        Keyword Arguments:
        record (VCFRecord): a VCF record
        """
        while record.POS > self.current_segment.end:
            self.current_segment_index += 1
            self.current_segment = (
                self.current_contig_segments[self.current_segment_index])
            pos = record.POS if record.POS < self.current_segment.end else None
            new_window = HomozygosityWindow.factory(
                record.CHROM,
                pos,
                self.current_segment,
                self.window_size,
                self.step_size,
                nsnps=True
            )
            self.windows.append(new_window)

