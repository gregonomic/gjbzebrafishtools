HZSCORER_MUT_IS_HET = 1 << 0
HZSCORER_MUT_IS_HOM = 1 << 1
HZSCORER_SIB_IS_HET = 1 << 2
HZSCORER_MUT_IS_HOM_INF = 1 << 3
HZSCORER_POSITION_IS_INFORMATIVE = 1 << 4
HZSCORER_MUT_IS_HOM_ENU = 1 << 5
HZSCORER_MUT_MISSING_MAPPING = 1 << 6
HZSCORER_MUT_HAS_MAPPING = 1 << 7
HZSCORER_REF_IS_HET = 1 << 8

class HomozygosityScorer(object):
    """Base class for homozygosity scorers.
    
    Homozygosity scorers take each record and return a key to increment in the
    surrounding window. They also take the final counts in a window and
    calculate the scroe for that window. Subclasses must implement
    key_to_increment(record) and score_from_counts(counts) methods.
    """
    
    name = None # Subclasses must override this
    
    @classmethod
    def customize_parser(self, parser):
        raise NotImplementedError("customize_parser() not implemented")

    def __init__(self, mutant_name, minimum_coverage=1,
        all_over_minimum_coverage=False):
        """
        Arguments:
            mutant_name (str) : name of mutant sample in VCF
        
        Keyword Arguments:
            minimum_coverage (int) : minimum coverage at which sample genotypes
                are taken into consideration
            all_over_minimum_coverage (bool) : require that all samples be over
                minimum_coverage for record to be scored
        """
        
        self.mutant_name = mutant_name
        self.minimum_coverage = minimum_coverage
        self.all_over_minimum_coverage = all_over_minimum_coverage
        self.informative_key = None # Subclasses must define this property

    def get_covered_allele_indices(self, gts):
        """Get indices of alleles that are covered in all gts
        
        Arguments:
            gts (list) : list of VCFRecord genotypes
        
        Returns:
            set : covered allele indices
        """
        covered_indices = set()
        for gt in gts:
            if gt['AD'] is not None:
                covered_indices.update(
                    [index for index, depth in enumerate(gt['AD'])
                    if depth > 0])
        return covered_indices

    def total_coverage_from_gts(self, gts):
        """Calculate total coverage for all alleles of all sample genotypes
        
        Arguments:
            gts (list) : list of VCFRecord genotypes
        
        Returns:
            int : total coverage
        """
        total_coverage = 0
        for gt in gts:
            if gt['AD'] is not None:
                total_coverage += sum(gt['AD'])
        return total_coverage

    def all_genotypes_are_over_minimum_coverage(self, gts):
        """Check whether all genotypes are over the specified minimum coverage
        
        Arguments:
            gts (list) : list of VCFRecord genotypes
        
        Returns:
            False if any genotype is below coverage, else True
        """
        for gt in gts:
            if self.total_coverage_from_gts([gt]) < self.minimum_coverage:
                return False
        return True

    def record_to_gt_dict(self, record):
        """Create a dictionary of genotypes for each group of samples.
        
        Subclasses should extend this method to add genotypes for relevant
        groups of samples.
        
        Arguments:
            record (VCFRecord) : a PyVCF VCFRecord instance
            
        Returns:
            a dict containing the mutant genotype with key 'mutant'
        """
        return {'mutant': [record.genotype(self.mutant_name)]}

    def check_samples(self, samples):
        """Check that samples are in VCF samples
        
        Subclasses should extend this method for other samples.
        
        Arguments:
            samples (list): sample names from VCF
        
        Return:
            list of sample errors
        """
        errors = []
        if self.mutant_name not in samples:
            errors.append(
                "Mutant '{0}' not found in VCF samples: {1}".format(
                    self.mutant_name,
                    ", ".join(samples)
                )
            )
        return errors

    def key_to_increment(self, record):
        """Take a VCF record and return a key to increment in the surrounding
        window's counts.
        
        Subclasses must override this method.
        
        Arguments:
            record (VCFRecord) : a PyVCF VCFRecord instance
            
        Returns:
            (int, str, ...) : key to increment in counts
        """
        raise NotImplementedError("key_to_increment() not implemented")

    def score_from_counts(self, counts):
        """Take a counter and return a score for the window.
        
        Subclasses must override this method.
        
        Arguments:
            counts (Counter) : a Counter instance, containing counts of interest
        
        Returns:
            float : score for window
        """
        raise NotImplementedError("score_from_counts() not implemented")

class MutantSibScorer(HomozygosityScorer):
    """A homozygosity scorer for mutant vs sibling pool comparisons"""
    
    name = 'MutSibScorer'
    
    @classmethod
    def customize_parser(self, parser):
        parser.add_argument(
            "-S",
            "--sibling-id",
            action="store",
            required=True,
            help="id of unaffected sibling"
        )

    def __init__(self, general_args, scorer_args):
        """Extends super class' __init__().
        
        Arguments:
            sibling_name (str) : name of sibling sample in VCF
        """
        super(MutantSibScorer, self).__init__(
            general_args.mutant_id,
            minimum_coverage=general_args.minimum_coverage,
            all_over_minimum_coverage=general_args.all_over_minimum_coverage
        )
        self.informative_key = HZSCORER_MUT_IS_HOM_INF
        self.sibling_name = scorer_args.sibling_id
    
    def check_samples(self, samples):
        """Extends super class' check_samples()"""
        errors = super(MutantSibScorer, self).check_samples(samples)
        if self.sibling_name not in samples:
            errors.append(
                "Sibling '{0}' not found in VCF samples: {1}".format(
                    self.sibling_name,
                    ", ".join(samples)
                )
            )
        return errors
        
    def key_to_increment(self, record):
        """Overrides super class' key_to_increment()"""
        key = 0
        gts_dict = self.record_to_gt_dict(record)
        
        # Check mutant is covered to sufficient depth
        if (self.total_coverage_from_gts(gts_dict['mutant'])
                < self.minimum_coverage):
            return key
        
        # Get covered allele indices for mutant, and increment het count if
        # appropriate
        mutant_covered_allele_indices = self.get_covered_allele_indices(
            gts_dict['mutant'])
        if len(mutant_covered_allele_indices) > 1:
            key = key | HZSCORER_MUT_IS_HET

        # Check sibling is covered to sufficient depth
        if (self.total_coverage_from_gts(gts_dict['sibling'])
                < self.minimum_coverage):
            return key

        # Get covered allele indices for sibling, and increment het count if
        # appropriate
        sibling_covered_allele_indices = self.get_covered_allele_indices(
            gts_dict['sibling'])
        if len(sibling_covered_allele_indices) > 1:
            key = key | HZSCORER_SIB_IS_HET

        # Check if mutant is homozygous informative, ie. it is homozygous for
        # an allele that is present in the sibling
        if (len(mutant_covered_allele_indices) == 1 and
                len(sibling_covered_allele_indices) > 1):
            mutant_allele_index = mutant_covered_allele_indices.pop()
            if mutant_allele_index in sibling_covered_allele_indices:
                key = key | HZSCORER_MUT_IS_HOM_INF
        return key

    def score_from_counts(self, counts):
        """Overrides super class' score_from_counts()"""
        sib_is_het = 1
        mut_is_het = 1
        mut_is_hom_inf = 1
        for key in counts:
            if key & HZSCORER_SIB_IS_HET:
                sib_is_het += counts[key]
            if key & HZSCORER_MUT_IS_HET:
                mut_is_het += counts[key]
            if key & HZSCORER_MUT_IS_HOM_INF:
                mut_is_hom_inf += counts[key]
                    
        return (float(sib_is_het) / float(mut_is_het)) * float(mut_is_hom_inf)

    def record_to_gt_dict(self, record):
        """Extends super class' record_to_gt_dict()"""
        gts_dict = super(MutantSibScorer, self).record_to_gt_dict(record)
        gts_dict['sibling'] = [record.genotype(self.sibling_name)]
        return gts_dict

class MutantRefScorerBase(HomozygosityScorer):
    """Base homozygosity scorer class for mutant vs reference strain
    comparisons.
    
    Extends HomozygosityScorer.
    """
    
    name = None

    @classmethod
    def customize_parser(self, parser):
        parser.add_argument(
            "-M",
            "--mapping_strain_ids",
            nargs='+',
            required=True,
            help="mapping strain IDs"
        )
        parser.add_argument(
            "-E",
            "--enu_ids",
            nargs='+',
            required=True,
            help="ENU strain IDs"
        )
        
    def __init__(self, mutant_name, enu_names, mapping_names, **kwargs):
        """Extends super class' __init__()
        
        Arguments:
            enu_names (list)     : list of strings of names of ENU (ie.
                mutagenised) strain samples
            mapping_names (list) : list of strings of names of mapping strain
                samples
        """
        super(MutantRefScorerBase, self).__init__(mutant_name, **kwargs)
        self.enu_names = enu_names
        self.mapping_names = mapping_names

    def check_samples(self, samples):
        errors = super(MutantRefScorerBase, self).check_samples(samples)
        
        # Check that mapping samples exist in VCF
        mapping_samples_not_found = [s for s in self.mapping_names
            if s not in samples]
        if len(mapping_samples_not_found) > 0:
            errors.append(
                "Mapping strain samples {0} not found in VCF samples: {1}".format(
                    ", ".join(mapping_samples_not_found),
                    ", ".join(samples)
                )
            )
            
        # Check that ENU samples exist in VCF
        enu_samples_not_found = [s for s in self.enu_names if s not in samples]
        if len(enu_samples_not_found) > 0:
            errors.append(
                "ENU strain samples {0} not found in VCF samples: {1}".format(
                    ", ".join(enu_samples_not_found),
                    ", ".join(samples)
                )
            )
        
        return errors

    def record_to_gt_dict(self, record):
        """Extends super class' record_to_gt_dict()"""
        gts_dict = super(MutantRefScorerBase, self).record_to_gt_dict(record)
        gts_dict['enu'] = [record.genotype(es) for es in self.enu_names]
        gts_dict['mapping'] = [record.genotype(ms) for ms in self.mapping_names]
        return gts_dict

class MutantRefIMBScorer(MutantRefScorerBase):
    """A homozygosity scorer for mutant vs reference (mutagenised and mapping
    strain) pool comparisons, using IMB score"""

    name = 'MutRefIMBScorer'
    
    def __init__(self, general_args, scorer_args):
        """Extends super class' __init__()"""
        super(MutantRefIMBScorer, self).__init__(
            general_args.mutant_id,
            scorer_args.enu_ids,
            scorer_args.mapping_strain_ids,
            minimum_coverage=general_args.minimum_coverage,
            all_over_minimum_coverage=general_args.all_over_minimum_coverage
        )
        self.informative_key = HZSCORER_POSITION_IS_INFORMATIVE

    def key_to_increment(self, record):
        """Overrides super class' key_to_increment()"""
        key = 0
        gts_dict = self.record_to_gt_dict(record)
        
        # Check mutant is covered to sufficient depth
        if (self.total_coverage_from_gts(gts_dict['mutant'])
                < self.minimum_coverage):
            return key

        # Get covered allele indices for mutant
        mutant_covered_allele_indices = self.get_covered_allele_indices(
            gts_dict['mutant'])
            
        # Add to hom or het mutant counts
        if len(mutant_covered_allele_indices) > 1:
            key = key | HZSCORER_MUT_IS_HET
        else:
            key = key | HZSCORER_MUT_IS_HOM

        # Check enu and mapping strain samples are all over minimum coverage,
        # if requested
        if self.all_over_minimum_coverage:
            if ((len(gts_dict['enu']) > 1) and
                    not self.all_genotypes_are_over_minimum_coverage(
                        gts_dict['enu'])):
                return key

            if ((len(gts_dict['mapping']) > 1) and
                    not self.all_genotypes_are_over_minimum_coverage(
                        gts_dict['mapping'])):
                return key

        # Check enu and mapping strain are covered to sufficient depth
        if ((self.total_coverage_from_gts(gts_dict['enu'])
                < self.minimum_coverage) or
                (self.total_coverage_from_gts(gts_dict['mapping'])
                < self.minimum_coverage)):
            return key

        # Get covered allele indices for ENU strain, and mapping strain
        enu_covered_allele_indices = self.get_covered_allele_indices(
            gts_dict['enu'])
        mapping_strain_covered_allele_indices = self.get_covered_allele_indices(
            gts_dict['mapping'])

        # Get informative ENU strain allele indices
        enu_inf_allele_indices = enu_covered_allele_indices.difference(
            mapping_strain_covered_allele_indices)
        # Get ENU strain-specific alleles in mutant
        enu_in_mutant = enu_inf_allele_indices.intersection(
            mutant_covered_allele_indices)

        # Determine whether there are any ENU strain-informative alleles
        if len(enu_inf_allele_indices) > 0:
            key = key | HZSCORER_POSITION_IS_INFORMATIVE

        # Check if mutant is hom_enu
        if len(mutant_covered_allele_indices) == 1 and len(enu_in_mutant) == 1:
            key = key | HZSCORER_MUT_IS_HOM_ENU

        return key

    def score_from_counts(self, counts):
        """Overrides super class' score_from_counts()"""
        pos_is_inf = 1
        mut_is_het = 1
        mut_is_hom_enu = 1
        for key in counts:
            if key & HZSCORER_POSITION_IS_INFORMATIVE:
                pos_is_inf += counts[key]
            if key & HZSCORER_MUT_IS_HET:
                mut_is_het += counts[key]
            if key & HZSCORER_MUT_IS_HOM_ENU:
                mut_is_hom_enu += counts[key]

        return (float(pos_is_inf) / float(mut_is_het)) * float(mut_is_hom_enu)


class MutantRefIMB2Scorer(MutantRefScorerBase):
    """A homozygosity scorer for mutant vs reference (mutagenised and mapping
    strain) pool comparisons, using IMB2 score"""

    name = 'MutRefIMB2Scorer'

    def __init__(self, general_args, scorer_args):
        """Extends super class' __init__()"""
        super(MutantRefIMB2Scorer, self).__init__(
            general_args.mutant_id,
            scorer_args.enu_ids,
            scorer_args.mapping_strain_ids,
            minimum_coverage=general_args.minimum_coverage,
            all_over_minimum_coverage=general_args.all_over_minimum_coverage
        )
        self.informative_key = HZSCORER_POSITION_IS_INFORMATIVE

    def key_to_increment(self, record):
        """Overrides super class' key_to_increment()"""
        key = 0
        gts_dict = self.record_to_gt_dict(record)
        
        # Check mutant is covered to sufficient depth
        if (self.total_coverage_from_gts(gts_dict['mutant'])
                < self.minimum_coverage):
            return key
        
        # Get covered allele indices for mutant
        mutant_covered_allele_indices = self.get_covered_allele_indices(
            gts_dict['mutant'])
        # Add to hom or het mutant counts
        if len(mutant_covered_allele_indices) > 1:
            key = key | HZSCORER_MUT_IS_HET
        else:
            key = key | HZSCORER_MUT_IS_HOM

        # Check enu and mapping strain samples are all over minimum coverage,
        # if requested
        if self.all_over_minimum_coverage:
            if ((len(gts_dict['enu']) > 1) and
                    not self.all_genotypes_are_over_minimum_coverage(
                        gts_dict['enu'])):
                return key

            if ((len(gts_dict['mapping']) > 1) and
                    not self.all_genotypes_are_over_minimum_coverage(
                        gts_dict['mapping'])):
                return key

        # Check enu and mapping strain are covered to sufficient depth
        if ((self.total_coverage_from_gts(gts_dict['enu'])
                < self.minimum_coverage) or
                (self.total_coverage_from_gts(gts_dict['mapping'])
                < self.minimum_coverage)):
            return key

        # Get covered allele indices for ENU strain, and mapping strain
        enu_covered_allele_indices = self.get_covered_allele_indices(
            gts_dict['enu'])
        mapping_strain_covered_allele_indices = self.get_covered_allele_indices(
            gts_dict['mapping'])

        # Get informative ENU strain allele indices
        enu_inf_allele_indices = enu_covered_allele_indices.difference(
            mapping_strain_covered_allele_indices)
        # Get ENU strain-specific alleles in mutant
        enu_in_mutant = enu_inf_allele_indices.intersection(
            mutant_covered_allele_indices)

        # Determine whether the ENU and mapping strains combined are
        # heterozygous
        enu_and_mapping_covered_allele_indices = (
            enu_covered_allele_indices.union(
                mapping_strain_covered_allele_indices))
        if len(enu_and_mapping_covered_allele_indices) > 1:
            key = key | HZSCORER_REF_IS_HET

        # Determine whether there are any ENU strain-informative alleles
        if len(enu_inf_allele_indices) > 0:
            key = key | HZSCORER_POSITION_IS_INFORMATIVE
        # Check if mutant is hom_enu
        if len(mutant_covered_allele_indices) == 1 and len(enu_in_mutant) == 1:
            key = key | HZSCORER_MUT_IS_HOM_ENU
        return key

    def score_from_counts(self, counts):
        """Overrides super class' score_from_counts()"""
        ref_is_het = 1
        mut_is_het = 1
        mut_is_hom_enu = 1
        for key in counts:
            if key & HZSCORER_REF_IS_HET:
                ref_is_het += counts[key]
            if key & HZSCORER_MUT_IS_HET:
                mut_is_het += counts[key]
            if key & HZSCORER_MUT_IS_HOM_ENU:
                mut_is_hom_enu += counts[key]

        return (float(ref_is_het) / float(mut_is_het)) * float(mut_is_hom_enu)

class MutantRefHenkeScorer(MutantRefScorerBase):
    """A homozygosity scorer for mutant vs reference (mutagenised and mapping
    strain) pool comparisons, using Henke et al score"""

    name = 'MutRefHenkeScorer'

    def __init__(self, general_args, scorer_args):
        """Extends super class' __init__()"""
        super(MutantRefHenkeScorer, self).__init__(
            general_args.mutant_id,
            scorer_args.enu_ids,
            scorer_args.mapping_strain_ids,
            minimum_coverage=general_args.minimum_coverage,
            all_over_minimum_coverage=general_args.all_over_minimum_coverage
        )
        self.informative_key = HZSCORER_POSITION_IS_INFORMATIVE

    def key_to_increment(self, record):
        """Overrides super class' key_to_increment()"""
        key = 0
        gts_dict = self.record_to_gt_dict(record)
        
        # Check mutant is covered to sufficient depth
        if (self.total_coverage_from_gts(gts_dict['mutant'])
                < self.minimum_coverage):
            return key

        # Get covered allele indices for mutant
        mutant_covered_allele_indices = self.get_covered_allele_indices(
            gts_dict['mutant'])
        # Add to hom or het mutant counts
        if len(mutant_covered_allele_indices) > 1:
            key = key | HZSCORER_MUT_IS_HET
        else:
            key = key | HZSCORER_MUT_IS_HOM

        # Check enu and mapping strain samples are all over minimum coverage,
        # if requested
        if self.all_over_minimum_coverage:
            if ((len(gts_dict['enu']) > 1) and
                    not self.all_genotypes_are_over_minimum_coverage(
                        gts_dict['enu'])):
                return key

            if ((len(gts_dict['mapping']) > 1) and
                    not self.all_genotypes_are_over_minimum_coverage(
                        gts_dict['mapping'])):
                return key

        # Check enu and mapping strain are covered to sufficient depth
        if ((self.total_coverage_from_gts(gts_dict['enu'])
                < self.minimum_coverage) or
                (self.total_coverage_from_gts(gts_dict['mapping'])
                < self.minimum_coverage)):
            return key

        # Get covered allele indices for ENU strain, and mapping strain
        enu_covered_allele_indices = self.get_covered_allele_indices(
            gts_dict['enu'])
        mapping_strain_covered_allele_indices = self.get_covered_allele_indices(
            gts_dict['mapping'])

        # Get informative mapping strain allele indices
        mapping_strain_inf_allele_indices = (
            mapping_strain_covered_allele_indices.difference(
                enu_covered_allele_indices))
        # Determine whether there are any ENU strain-informative alleles
        if len(mapping_strain_inf_allele_indices) > 0:
            key = key | HZSCORER_POSITION_IS_INFORMATIVE

        # Get mapping strain-specific alleles in mutant
        mapping_strain_in_mutant = (
            mapping_strain_inf_allele_indices.intersection(
                mutant_covered_allele_indices))

        # Check whether any of the mapping strain informative alleles are
        # missing in the mutant
        if (len(mapping_strain_in_mutant)
                < len(mapping_strain_inf_allele_indices)):
            # At least 1 mapping strain allele is missing in mutant
            key = key | HZSCORER_MUT_MISSING_MAPPING

        # Check if any mapping strain informative alleles are in mutant
        if len(mapping_strain_in_mutant) > 0:
            key = key | HZSCORER_MUT_HAS_MAPPING

        return key

    def score_from_counts(self, counts):
        """Overrides super class' score_from_counts()
            
        From Henke et al., Current Protocols. Volume 104, Issue 1, October 2013,
        Pages 7.13.1-7.13.33:
        
        A linked region should be characterized by a low level of heterogeneous
        SNPs and mapping strain specific alleles (tracks 1 and 6) and a high
        level of homogeneous SNPs and alleles specific to the mutagenized strain
        (tracks 2 and 4). These characteristics will also be represented by a
        high mapping score. The mapping score is calculated as follows in 20-cM
        sliding windows:

         # homogeneous SNPs    #mapping strain-specific alleles not found in the mutant
        -------------------- x --------------------------------------------------------
        # heterogeneous SNPs     #mapping strain-specific alleles found in the mutant

        """
        mut_is_hom = 1
        mut_is_het = 1
        mut_miss_map = 1
        mut_has_map = 1
        for key in counts:
            if key & HZSCORER_MUT_IS_HOM:
                mut_is_hom += counts[key]
            if key & HZSCORER_MUT_IS_HET:
                mut_is_het += counts[key]
            if key & HZSCORER_MUT_MISSING_MAPPING:
                mut_miss_map += counts[key]
            if key & HZSCORER_MUT_HAS_MAPPING:
                mut_has_map += counts[key]
                    
        return ((float(mut_is_hom) / float(mut_is_het))
            * (float(mut_miss_map) / float(mut_has_map)))
