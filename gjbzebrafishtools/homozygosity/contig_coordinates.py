from collections import namedtuple

ContigSegment = namedtuple('ContigSegment', 'start, end')

class VCFContig(object):
    """A class to represent a VCF contig, including gap coordinates"""
    
    def __init__(self, name, length):
        """
        Arguments:
            name (str)   : contig name
            length (int) : contig length
        
        Attributes:
            name (str)   : contig name
            length (int) : contig length
            gaps (list)  : list of tuples of gap coordinates
        """
        self.name = name
        self.length = length
        self.gaps = []

#    def coords(self):
#        return [1, self.length]

    def gapped_coords(self):
        """Calculate and return contig coordinates split into segments based on gap coordinates
        
        Returns:
            list of ContigSegment tuples
        """
        if self.gaps is None or len(self.gaps) == 0:
            return [ContigSegment(1, self.length)]
        # There is at least 1 gap
        gapped_coords = []
        segment_start = 1
        segment_end = None
        # Sort gaps
        sorted_gaps = sorted(self.gaps, key=lambda g: g[0])
        # Make gapped coords
        for gap in sorted_gaps:
            gap_start = gap[0]
            gap_end = gap[1]
            # Sometimes, there will be 2 gaps next to each other
            if (gap_start < segment_start):
                segment_start = gap_end + 1
                continue
            segment_end = gap_start
            # Add segment to contig_coords
            gapped_coords.append(ContigSegment(segment_start, segment_end))
            # Adjust segment start
            segment_start = gap_end + 1
        # Make last contig and add to contig_coords
        segment_end = self.length
        gapped_coords.append(ContigSegment(segment_start, segment_end))
        return gapped_coords

class VCFContigCoordinates(object):
    """Controller for VCFContigs of a VCF"""
    
    def __init__(self, contigs=None):
        """
        Keyword Arguments:
            contigs (list) : list of VCFContigs
        """
        self.contigs = contigs

    def add_gaps_to_contigs(self, gaps):
        """Add gap coordinates to contigs
        
        Arguments:
            gaps (dict): dict with contig names as keys and their gap coordinates as values
        """
        for contig in self.contigs:
            if contig.name in gaps:
                contig.gaps = gaps[contig.name]

    def get_contig_with_name(self, name):
        """Return a VCFContig with name
        
        Arguments:
            contig name (str) : name of contig
        
        Returns:
            a contig (VCFContig) with name if found, else None
        """
        return next(iter([c for c in self.contigs if c.name == name]), None)

    def contig_names_list(self):
        """Return list of contig names
        
        Returns:
            (list) list of contig names
        """
        return [c.name for c in self.contigs]

    def total_length_of_contigs(self):
        """
        Returns:
            (int) total length of all contigs
        """
        return sum(c.length for c in self.contigs)

    def longest_contig_length(self):
        """
        Returns:
            (int) length of longest contig
        """
        return max(c.length for c in self.contigs)
