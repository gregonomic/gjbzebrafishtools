#!/usr/bin/env python

from __future__ import print_function

import sys
import os
import argparse
import pkg_resources

import csv
import vcf
from collections import Counter

from gjbzebrafishtools.homozygosity.homozygosity_window import (
    HomozygosityWindow,
    HomozygosityWindowController
)
from gjbzebrafishtools.homozygosity.contig_coordinates import (
    VCFContig,
    VCFContigCoordinates
)
#from scorer.homozygosity_scorer import MutantSibScorer, MutantRefIMBScorer, MutantRefIMB2Scorer, MutantRefHenkeScorer

VERSION_INFO = (0, 0, 1)
VERSION = '.'.join(str(c) for c in VERSION_INFO)

"""Argument parsing pattern adapted from
`PyVCF vcf_filter.py <https://pyvcf.readthedocs.io/en/latest/FILTERS.html#the-filter-script-vcf-filter-py>`"""

def create_scorer_parser(name):
    parser = argparse.ArgumentParser(
        description='Parser for %s' % name,
        add_help=False
    )

    return parser

def create_core_parser(scorer_names):
    # we have to use custom formatted usage, because of the
    # multi-stage argument parsing (otherwise the scorer arguments
    # are grouped together with the other optionals)

    # Get options
    parser = argparse.ArgumentParser(
        description='Perform homozygosity detection on input VCF of forward \
screen genotyping data',
        add_help=False,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        usage="""%(prog)s [-h] [--output-bedgraph <bg_filename>] [--gaps-bed
            </path/to/gaps/bed>] [--minimum-coverage <coverage>]
            [--all-over-minimum-coverage] [--use-nsnps-windows] [--window-size
            <window size>] [--step-size <step size>] [--write-bedgraph-header]
            [--write-bedgraph-trackline] [--list-samples] [--list-contigs]
              --input-vcf [input] [scorer [scorer args]]
        """)
    parser.add_argument('-h', '--help', action='store_true',
            help='Show this help message and exit.')

    # Add arguments common to all analyses
    parser.add_argument(
        "--input-vcf",
        metavar="input",
        type=argparse.FileType('rb'),
        nargs='?',
        default=None,
        help="File to process (use - for STDIN)"
    )
    parser.add_argument(
        "-m",
        "--mutant-id",
        action="store",
        help="id of mutant (ie. affected) sample"
    )
    parser.add_argument(
        "-o",
        "--output-bedgraph",
        action="store",
        default=sys.stdout,
        help="Filename to output [STDOUT]"
    )
    parser.add_argument(
        "-g",
        "--gaps-bed",
        action="store",
        help="BED file containing gaps coordinates; \
omit if you want to ignore gaps"
    )
    parser.add_argument(
        "-c",
        "--minimum-coverage",
        action="store",
        type=int,
        default=1,
        help="minimum coverage required"
    )
    parser.add_argument(
        "-a",
        "--all_over-minimum-coverage",
        action="store_true",
        default=False,
        help="if there are multiple require all samples to be >= minimum \
coverage required"
    )
    parser.add_argument(
        "-nsnps",
        "--use-nsnps-windows",
        action="store_true",
        default=False,
        help="define window and step sizes by number of informative SNPs; \
default is to define windows by number of basepairs"
    )
    parser.add_argument(
        "-w",
        "--window-size",
        action="store",
        type=int,
        help="size of window (integer), default=10000 (for nbp windows) or 200 \
(for nsnps windows)"
    )
    parser.add_argument(
        "-s",
        "--step-size",
        action="store",
        type=int,
        help="step size for window movement (integer), default=1000 (for nbp \
windows) or 20 (for nsnps windows)"
    )
    parser.add_argument(
        "-bgh",
        "--write-bedgraph-header",
        action="store_true",
        default=True,
        help="write bedgraph header line, (True)"
    )
    parser.add_argument(
        "-bgts",
        "--write-bedgraph-trackline",
        action="store_true",
        default=False,
        help="write bedgraph track line (for compatibility with UCSC genome \
browser), (False)"
    )
    # Add 'list-samples' action
    parser.add_argument(
        "-ls",
        "--list-samples",
        action="store_true",
        default=False,
        help="list VCF contigs and quit"
    )
    # Add 'list-contigs' action
    parser.add_argument(
        "-lc",
        "--list-contigs",
        action="store_true",
        default=False,
        help="list VCF contigs and quit"
    )
    parser.add_argument(
        "-sn",
        "--scorer-name",
        action="store",
        choices=scorer_names,
        help="scorer name <{0}>".format("|".join(scorer_names))
    )

    return parser

def main():
    """Perform homozygosity detection on input VCF of forward screen genotyping
    data.
    
    The input VCF should contain genotypes of a single mutant pool, as well as:
    * a single unaffected sibling pool
      OR
    * one or more ENU-strain (ie. the mutagenised) samples (or a pool thereof)
      and one or more mapping-strain (ie. the outcrossing strain) samples (or a
      pool thereof).
    
    The input VCF filename can be provided (in which case it must be an
    uncompressed VCF file), or VCF data can be piped in (eg. by reading from a
    BCF file with bcftools).
    
    Genotypes must have the 'AD' FORMAT field set. This is standard in GATK
    UnifiedGenotyper output, and can be generated in bcftools mpileup output by
    specifyting the option: -a "FORMAT/AD". Unfortunately, VarScan/VarScan2 uses
    'AD' to represent "Reads supporting variant..." (ie. the number of reads
    supporting the alternate [non-reference] allele), and as such
    VarScan/VarScan2 VCFs cannot be used.
    
    If a BED file containing coordinates of gaps in the assembly is provided
    (using the -g/--gaps_bed option), homozygosity windows will be terminated
    at gap start positions and initiated at gap end positions. Otherwise,
    homozygosity windows will ignore gaps. It is sometimes helpful to generate
    results both with and without -g/--gaps_bed, eg. in cases where the
    reference genome is mis-assembled.
    
    Output is in BedGraph format (see `UCSC Genome Browser BedGraph format
    <https://genome.ucsc.edu/goldenpath/help/bedgraph.html>` for details).
    
    To write a bedgraph header line (eg. '##label="<output_bedgraph>"'),
    use the -bgh/--write_bedgraph_header option. This will also write lines
    containing the input parameters to this script. This is highly recommended.
    
    To write a bedgraph trackline to enable viewing in the UCSC Genome Browser
    (eg. 'track type=bedGraph name="<output_bedgraph>"').
    
    Arguments:
        input_vcf (str) : path to VCF file, or '-' if piping from eg. bcftools
        mutant_id (str) : name of mutant sample
        
    Keyword Arguments:
        -o/--output-bedgraph (str)     : name of bedgraph file to which to
            write; default: stdout
        -g/--gaps-bed (str)            : name of bed file containing gap
            coordinates; default: gaps ignored
        -c/--minimum-coverage (int)    : minimum coverage of samples required
            for them to be considered in calculations; for enu_ids and
            mapping_ids, the total coverage for all samples is used
        -a/--all-over-minimum-coverage : require all ENU and mapping samples to
            be over minimum_coverage
        -nsnps/--use-nsnps-windows        : define windows by number of
            (informative) SNPs; default: define windows by number of base-pairs
        -w/--window-size (int)         : window size
    """

    # dynamically build the list of available scorers
    scorers = {}
    
    # look for global extensions
    for p in pkg_resources.iter_entry_points('homozygosity.scorers'):
        scorer = p.load()
        scorers[scorer.name] = scorer

    # parse command line args
    parser = create_core_parser(scorers.keys())
    (args, unknown_args) = parser.parse_known_args()

    # If list-samples or list-contigs, do so, and quit
    if args.list_samples or args.list_contigs:
        # Open VCF
        vcf_reader = vcf.Reader(args.input_vcf)
        if args.list_samples:
            # List samples and quit
            print(" ".join(vcf_reader.samples))
        # List contigs and quit
        elif args.list_contigs:
            print("\n".join(c.id for c in vcf_reader.contigs.values()))
        return

    # Check if scorer_name provided
    if args.scorer_name:
        # Scorer selected, add arguments from scorer
        s = scorers[args.scorer_name]
        arg_group = parser.add_argument_group(s.name, scorer.__doc__)
        s.customize_parser(arg_group)
    else:
        # Scorer not selected, print help
        parser.print_help()
        parser.exit()

    # OK, looks like a scorer name was provided. Get the scorer from scorers
    # dict, make its parser, and parse its args
    scorer_class = scorers[args.scorer_name]
    scorer_parser = create_scorer_parser(args.scorer_name)
    scorer_class.customize_parser(scorer_parser)
    (known_scorer_args, unknown_scorer_args) = scorer_parser.parse_known_args(
        unknown_args)
    if len(unknown_scorer_args):
        sys.exit("%s has no arguments like %s" %
            (scorer.name, unknown_scorer_args))

    # print help using the 'help' parser, so it includes
    # all possible filters and arguments
    if args.help or (args.input_vcf == None):
        parser.print_help()
        parser.exit()

#    # Open VCF
    vcf_reader = vcf.Reader(args.input_vcf)

    scorer = scorer_class(args, known_scorer_args)
    samples = vcf_reader.samples
    sample_errors = scorer.check_samples(samples)
    if len(sample_errors) > 0:
        sys.exit("\n".join(sample_errors))

    # Set default window and step sizes
    if args.use_nsnps_windows:
        if not args.window_size:
            args.window_size = 200
        if not args.step_size:
            args.step_size = 20
    else:
        if not args.window_size:
            args.window_size = 10000
        if not args.step_size:
            args.step_size = 1000

    parse_vcf(vcf_reader, scorer, args)

# MARK: Parse VCF methods

def parse_vcf(vcf_reader, scorer, args):
    """Parse the VCF, writing homozygosity scores at the end of each contig
    
    Arguments:
        vcf_reader (VCF.reader)        : a VCF.reader instance, opened for
            reading
        args (argparse.ArgumentParser) : the main argument parser
    """
    
    # Initialise contig_coords
    contigs_l = [VCFContig(name=c[0], length=int(c[1]))
        for c in vcf_reader.contigs.values()]
    contig_coords = VCFContigCoordinates(contigs=contigs_l)
    if args.gaps_bed:
        gaps = load_gaps_from_bed_file(args.gaps_bed)
        if len(gaps) == 0:
            sys.exit("Loading of gap coords failed. Quitting.")
        contig_coords.add_gaps_to_contigs(gaps)

    # Initialise the window controller
    window_controller = HomozygosityWindowController.factory(
        args.use_nsnps_windows,
        window_size=args.window_size,
        step_size=args.step_size,
        contig_coords=contig_coords,
        scorer=scorer
    )

    # Open bedgraph and print header lines and track lines, if requested
    bedgraph_fh = open(args.output_bedgraph, 'w')
    
    if args.write_bedgraph_header:
        vcf_filename = (vcf_reader.filename if vcf_reader.filename != '<stdin>'
            else get_vcf_filename(vcf_reader))
        print(
            bedgraph_header_str(args, scorer, vcf_filename),
            file=bedgraph_fh
        )
        print(
            "#label=\"{0}\"".format(
                args.output_bedgraph.replace(".bedgraph","")
            ),
            file=bedgraph_fh
        )

    if args.write_bedgraph_trackline:
        print(
            "track type=bedGraph name=\"{0}\"".format(
                args.output_bedgraph.replace(".bedgraph","")
            ),
            file=bedgraph_fh
        )
    

    # Parse the VCF
    for record in vcf_reader:
        windows_to_write = window_controller.process_vcf_record(record)
        if windows_to_write is not None:
            write_window_scores_to_bedgraph_file(windows_to_write, bedgraph_fh)

    # Write the windows for the last contig
    window_controller.set_scores()
    write_window_scores_to_bedgraph_file(window_controller.windows, bedgraph_fh)

    # Close bedgraph
    if bedgraph_fh is not None:
        bedgraph_fh.close()

def get_vcf_filename(vcf_reader):
    """Attempt to extract VCF filename from VCF header metadata
    """
    try:
        bcftools_view_cmds = vcf_reader.metadata.get('bcftools_viewCommand',
            None)
        # If this was piped from bcftools, the view command will be something
        # like 'view bcffilename.bcf' or 'view bcffilename.bcf chr:start-end'
        last_cmd = bcftools_view_cmds[-1]
        return " ".join(last_cmd.split(" ")[1:])
    except Exception as e:
        return None

def bedgraph_header_str(args, scorer, vcf_filename):
    """Create header for bedgraph file, with run parameters
    
    Arguments:
        args (ArgParser)            : the main arguments
        scorer (HomozygosityScorer) : a homozygosity scorer
        
    Returns:
        str : multi-line bedgraph header string
    """
    window_type = 'nsnps' if args.use_nsnps_windows else 'nbps'
    terminate_windows_at_gaps = "True" if args.gaps_bed else "False"
    gaps_bed = args.gaps_bed if args.gaps_bed else "N/A"
    bedgraph_header_lines = [
        "##This bedgraph file was generated by GJBZebrafishTools \
{0} version {1}".format(os.path.basename(__file__), VERSION),
        "##foshzl_input-vcf={0}".format(vcf_filename),
        "##foshzl_scorer={0}".format(scorer.__class__.__name__),
        "##foshzl_window-type={0}".format(window_type),
        "##foshzl_window-size={0}".format(args.window_size),
        "##foshzl_step-size={0}".format(args.step_size),
        "##foshzl_minimum-coverage={0}".format(args.minimum_coverage),
        "##foshzl_gaps-bed-file={0}".format(gaps_bed)
    ]
    return "\n".join(bedgraph_header_lines)

def load_gaps_from_bed_file(bed_filename):
    """Load gaps from bedgraph filename
    
    Arguments:
        bed_filename (str) : name of the file containing gap coordinates
            (tab-separated, in
            `BED format <https://genome.ucsc.edu/FAQ/FAQformat.html#format1>`:
            chrom<tab>chromStart<tab>chromEnd)
    
    Returns:
        dict : contig names as keys and arrays of gap coordinates as values
    """
    gaps = {}
    
#    # Initialise currentContig
#    current_contig = None
#    current_contig_gaps = []

    # Initialise a file reader
    with open(bed_filename, 'r') as bfh:
    
        bg_reader = csv.reader(bfh, delimiter="\t")
        
        # Parse file
        for line in bg_reader:
        
            # Skip comment lines
            if line[0].startswith('#'):
                continue
        
            # Set contig_name, gap_start, gap_end
            contig_name = line[0]
            gap_start = int(line[1])
            gap_end = int(line[2])

            # Check if we've already initialised a list for this contig
            if contig_name not in gaps:
                gaps[contig_name] = []

            # Add this gap
            gaps[contig_name].append([gap_start, gap_end])

#            # Check if we've changed contigs
#            if current_contig is None or contig_name != current_contig:
#                # We have, save the gaps from prev contig
#                if current_contig is not None:
#                    gaps[current_contig] = current_contig_gaps
#                current_contig = contig_name
#                current_contig_gaps = []
#                
#            current_contig_gaps.append([gap_start, gap_end])
#
#    # Set gaps for last contig
#    if current_contig is not None:
#        gaps[current_contig] = current_contig_gaps

    return gaps

def write_window_scores_to_bedgraph_file(windows_to_write, bedgraph_fh):
    """Write window scores to open bedgraph filehandle
    
    Arguments:
        windows_to_write (list) : list of homozygosity windows to write
        bedgraph_fh (filehandle): bedgraph filehandle opened for writing
    """
    n_windows = len(windows_to_write)
    for index, window in enumerate(windows_to_write):
        prev_window = windows_to_write[index - 1] if index > 0 else None
        next_window = (windows_to_write[index + 1]
            if index + 1 < n_windows else None)
        window_bg_str = window.bedgraph_string(prev_window, next_window)
        if window_bg_str is not None:
            print(window_bg_str, file=bedgraph_fh)

#-------------------------------
if __name__ == "__main__":
    main()

