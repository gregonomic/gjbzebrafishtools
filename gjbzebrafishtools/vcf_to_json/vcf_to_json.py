#!/usr/bin/env python

from __future__ import print_function

import sys
import argparse
import vcf
import json
import copy
import pkg_resources
import pymongo
from pymongo import MongoClient

VERSION_INFO = (0, 0, 1)
VERSION = '.'.join(str(c) for c in VERSION_INFO)

def create_rec_parser_argparser(name):
    parser = argparse.ArgumentParser(
        description='Parser for %s' % name,
        add_help=False
    )
    parser.add_argument('rest',
        nargs=argparse.REMAINDER,
        help=argparse.SUPPRESS
    )

    return parser

def create_core_argparser(parser_names):
    # we have to use custom formatted usage, because of the
    # multi-stage argument parsing (otherwise the rec parser arguments
    # are grouped together with the other optionals)
    parser = argparse.ArgumentParser(
        description='Convert a VCF file to JSON or MongoDB',
        add_help=False,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        usage="%(prog)s [-h] [-ai|-wi wanted_info [-wi wanted_info ...]] \
[-af|-wf wanted_format [-wf wanted_format ...]] \
input <list_samples|list_contigs|list_infos|list_formats|to_json|to_mongo> \
[parser [parser_args]] [parser [parser_args]] ... where parser is one of {0}\
        ".format("|".join(parser_names))
    )
    parser.add_argument(
        '-h',
        '--help',
        action='store_true',
        help='Show this help message and exit.'
    )
    parser.add_argument(
        'input',
        metavar='input',
        type=argparse.FileType('rb'),
        nargs='?',
        default=None,
        help='File to process (use - for STDIN)'
    )
    
    # Create group for choosing formats
    infos_parser = parser.add_mutually_exclusive_group()
    infos_parser.add_argument(
        "-ai",
        "--all-infos",
        action="store_true",
        help="add all info fields to output"
    )
    infos_parser.add_argument(
        "-wi",
        "--wanted-infos",
        action="append",
        help="keys of wanted infos"
    )

    # Create group for choosing formats
    formats_parser = parser.add_mutually_exclusive_group()
    formats_parser.add_argument(
        "-af",
        "--all-formats",
        action="store_true",
        help="add all FORMAT fields to per-sample output"
    )
    formats_parser.add_argument(
        "-wf",
        "--wanted-formats",
        action="append",
        help="only add selected FORMAT fields to per-sample output"
    )
    
    # Create group for choosing list_samples, list_contigs, list_infos,
    # to_json, or to_mongo
    action_group = parser.add_mutually_exclusive_group(required=True)
    action_group.add_argument(
        "--list-samples",
        action="store_true",
        help="list VCF samples and quit"
    )
    action_group.add_argument(
        "--list-contigs",
        action="store_true",
        help="list VCF contigs and quit"
    )
    action_group.add_argument(
        "--list-infos",
        action="store_true",
        help="list VCF infos and quit"
    )
    action_group.add_argument(
        "--list-formats",
        action="store_true",
        help="list VCF sample format fields and quit"
    )
    action_group.add_argument(
        "--to-json-file",
        action="store",
        help="JSON filename into which to convert VCF records"
    )
    action_group.add_argument(
        "--to-mongo",
        action="store",
        help="MongoDB collection name into which to convert VCF records"
    )
    parser.add_argument(
        "--append",
        action="store_true",
        help="append to existing MongoDB collection; default is to remove all \
existing records before adding"
    )

    parser.add_argument(
        'rest',
        nargs=argparse.REMAINDER,
        help=argparse.SUPPRESS
    )

    return parser

# argument parsing strategy from `PyVCF <https://pyvcf.readthedocs.io/en/latest/>`

def main():
    """Convert VCF records to JSON representation.

    The input VCF filename can be provided (in which case it must be an
    uncompressed VCF file), or VCF data can be piped in (eg. by reading from a
    BCF file with bcftools).

    Arguments:
        input_vcf (str) : path to VCF file, or '-' if piping from eg. bcftools
        mutant_id (str) : name of mutant sample
    """

    # dynamically build the list of available rec parsers
    rec_parsers = {}

    # look for global extensions
    for p in pkg_resources.iter_entry_points('vcf_json_parsers'):
        rec_parser = p.load()
        rec_parsers[rec_parser.name] = rec_parser
#        add_rec_parser(rec_parser)

    # parse command line args
    parser = create_core_argparser(rec_parsers.keys())
    (args, unknown_args) = parser.parse_known_args()
    
    # add parser to dictionary, extend help message
    # with help/arguments of each rec parser
    def add_rec_parser(rec_parser):
        rec_parsers[rec_parser.name] = rec_parser
        arg_group = parser.add_argument_group(rec_parser.name,
                                              rec_parser.__doc__)
        rec_parser.customize_parser(arg_group)


    # go through the parsers on the command line
    # one by one, trying to consume only the declared arguments
    used_parsers = []
    while len(args.rest):
        rec_parser_name = args.rest.pop(0)
        if rec_parser_name not in rec_parsers:
            sys.exit("%s is not a known rec_parser (%s)" %
                (rec_parser_name, str(rec_parsers.keys())))

        # create an argparser only for arguments of current rec_parser
        rec_parser_argparser = create_rec_parser_argparser(rec_parser_name)
        rec_parsers[rec_parser_name].customize_parser(rec_parser_argparser)
        (known_rec_parser_args, unknown_rec_parser_args) = rec_parser_argparser.parse_known_args(
            args.rest)
        if len(unknown_rec_parser_args):
            sys.exit("%s has no arguments like %s" %
                (rec_parser_name, unknown_rec_parser_args))

        used_parsers.append((rec_parser_name, known_rec_parser_args))
        args.rest = known_rec_parser_args.rest

    # print help using the 'help' parser, so it includes
    # all possible rec parsers and arguments
    if (args.help or
            (len(used_parsers) == 0 and
            not args.all_infos and
            args.wanted_infos is None and
            (args.to_json_file or
            args.to_mongo)) or
            (args.input == None)):
        parser.print_help()
        parser.exit()

    # Open input VCF
    inp = vcf.Reader(args.input)

    # List samples and quit
    if args.list_samples:
        print(" ".join(inp.samples))
        return
    
    # List contigs and quit
    if args.list_contigs:
        print("\n".join(c.id for c in inp.contigs.values()))
        return

    # List infos and quit
    if args.list_infos:
        print("\n".join("{0}\t{1}".format(i.id, i.desc) for i in inp.infos.values()))
        return

    # List formats and quit
    if args.list_formats:
        print("\n".join("{0}\t{1}".format(f.id, f.desc) for f in inp.formats.values()))
        return

    # Must want to parse into JSON or MongoDB
    
    # Run some checks first
    if args.wanted_infos:
        missing_infos = [i for i in args.wanted_infos if i not in inp.infos]
        if len(missing_infos) > 0:
            sys.exit(
                "These wanted infos were not found in this VCF file: {0}".format(
                    ",".join(missing_infos)))

    if args.wanted_formats:
        missing_formats = [f for f in args.wanted_formats if f not in inp.formats]
        if len(missing_formats) > 0:
            sys.exit(
                "These wanted formats were not found in this VCF file: {0}".format(
                    ",".join(missing_formats)))

    # build rec parser chain
    chain = []
    for (name, rec_parser_args) in used_parsers:
        p = rec_parsers[name](rec_parser_args)
        chain.append(p)

    if args.to_json_file:
        to_json(args, inp, chain)

    if args.to_mongo:
        to_mongo(args, inp, chain)

# MARK: Parse VCF methods

def vcf_record_to_dict(record, vcf_reader, wanted_infos, all_infos,
        wanted_formats, all_formats, rec_parsers):
    """Convert a VCFRecord to a dict"""
    # Initialise dict with chrom and position
    d = {
        'chrom': record.CHROM,
        'position': record.POS
    }

    # Delete those key-value pairs that will be consumed by rec_parsers
    reserved_keys = [k for rp in rec_parsers for k in rp.reserved_keys()]

    # If wanted_infos, delete those that aren't wanted
    if wanted_infos is not None:
        d.update(
            get_infos(
                record.INFO,
                vcf_reader.infos,
                wanted_keys=wanted_infos,
                reserved_keys=reserved_keys
            )
        )
    # If all infos requested,
    elif all_infos:
        d.update(
            get_infos(
                record.INFO,
                vcf_reader.infos.keys(),
                reserved_keys=reserved_keys
            )
        )

    # Parse formats
    if all_formats or wanted_formats is not None:
        genotypes = get_genotypes(
                        record.samples,
                        vcf_reader.formats,
                        wanted_formats=wanted_formats)
        d['genotypes'] = genotypes

    # Finally, add keys & values for each parser
    for parser in rec_parsers:
        parser_d = parser(record)
        d[parser.name] = parser_d

    return d

def get_infos(record_infos, vcf_header_infos, wanted_keys=None,
        reserved_keys=None):
    """Create dict of info key/values"""
    d = {}
    if wanted_keys is None:
        if reserved_keys is None:
            wanted_keys = vcf_header_infos.keys()
        else:
            wanted_keys = [k for k in vcf_header_infos.keys()
                            if k not in reserved_keys]

    for k in wanted_keys:
        if vcf_header_infos[k].type == 'Flag':
            d[k] = k in record_infos
        else:
            d[k] = record_infos[k] if k in record_infos else None
    return d

def get_genotypes(record_samples, vcf_header_formats, wanted_formats=None):
    """Create list of genotypes"""
    genotypes = []
    if wanted_formats is None:
        wanted_formats = vcf_header_formats.keys()
    
    for s in record_samples:
        s_dict = {"name": s.sample}
        for wf in wanted_formats:
            s_dict[wf] = s.get(wf)
        genotypes.append(s_dict)
    return genotypes

def to_json(args, vcf_reader, rec_parsers):
    """Convert VCF to JSON"""
    
    results = []
    with open(args.to_json_file, 'w') as json_out:
        for record in vcf_reader:
            d = vcf_record_to_dict(
                record,
                vcf_reader,
                args.wanted_infos,
                args.all_infos,
                args.wanted_formats,
                args.all_formats,
                rec_parsers
            )
            results.append(d)
            
        json.dump(results, json_out, indent=4)

def to_mongo(args, vcf_reader, rec_parsers):
    """Convert VCF to MongoDB
    
    Arguments:
        args: main arguments
    """
    try:
        client = MongoClient(serverSelectionTimeoutMS=10)
    except pymongo.errors.ServerSelectionTimeoutError as err:
        sys.exit("Unable to open MongoDB database. Do you have MongoDB running?: {0}".format(err))
        
    db = client['test-database'] # or client.test_database
    collection = db[args.to_mongo] # or db.test_collection

    # If append not requested, delete existing records (to avoid duplication)
    if not args.append:
        result = collection.delete_many({})
        print("{0} records deleted".format(result.deleted_count))
    
    for record in vcf_reader:
        d = vcf_record_to_dict(
            record,
            vcf_reader,
            args.wanted_infos,
            args.all_infos,
            args.wanted_formats,
            args.all_formats,
            rec_parsers
        )
        # Finally, write to db
        d_id = collection.insert_one(d)

    # After all records have been written, create index on chrom and pos,
    # for faster retrieval
    result = db.profiles.create_index(
        [
            ('chrom', pymongo.ASCENDING),
            ('position', pymongo.ASCENDING),
        ]
    )

#-------------------------------
if __name__ == "__main__":
    main()


