import re

class SnpEffParser(object):
    """ Parse SnpEff information from VCFRecord into dict."""

    name = 'snpeff_parser'
    # Make dict for ranking impacts
    impacts_rank = {'MODIFIER': 0, 'LOW': 1, 'MODERATE': 2, 'HIGH': 3}
     # Initialise regular expression for parsing EFF strings
    re_pattern = re.compile("(.*)\((.*)\)")

    @classmethod
    def customize_parser(self, parser):
        parser.add_argument(
            "-sem",
            "--snpeff-mutant-id",
            action="store",
            required="true",
            help="id of mutant (ie. affected) sample"
        )
        parser.add_argument(
            "-see",
            "--snpeff-info-key",
            action="store",
            default='EFF',
            choices=['EFF', 'ANN'],
            help="INFO key that identifies snpEff annotation, either 'EFF' or \
'ANN'; default='EFF'"
        )
        parser.add_argument(
            "-semi",
            "--snpeff-minimum-impact",
            action="store",
            default="MODIFIER",
            choices=["HIGH", "MODERATE", "LOW", "MODIFIER"],
            help="minimum impact(s), options=['MODIFIER', 'LOW', 'MODERATE', \
'HIGH']; "
        )

    def __init__(self, args):
        self.mutant_id = args.snpeff_mutant_id
        self.snpeff_info_key = args.snpeff_info_key
        self.minimum_impact = args.snpeff_minimum_impact

    def __call__(self, record):
        """Parse the record.INFO for SnpEff results"""
        
        snpeff_result = (record.INFO[self.snpeff_info_key]
                         if self.snpeff_info_key in record.INFO else None)
        if snpeff_result is None:
            return None

        # Initialise dict for storing info
        d = {
            'effects':[],
            'highest_impact': None,
            'highest_impact_gene': None,
            'highest_impact_effect': None,
            'highest_impact_mutant_allele_depth_ratio': None
        }
        
        mutant_gt  = record.genotype(self.mutant_id)

        # Create dict for retrieving index of alleles of interest
        alleles = {str(a):i for i,a in enumerate(record.alleles)}
        
        # Now parse all SnpEff results, updating impact and adding them to effects list where appropriate
        for result in snpeff_result:
            e_i = self.re_pattern.search(result)
            effect = e_i.group(1)
            info   = e_i.group(2)
            if self.snpeff_info_key == 'EFF':
                # Parse old EFF format
                try:
                    (impact,
                    functional_class,
                    codon_change,
                    amino_acid_change,
                    amino_acid_length,
                    gene_name,
                    gene_biotype,
                    coding,
                    transcript,
                    exon,
                    genotype,
                    warnings_errors) = info.split('|')
                except:
                    (impact,
                    functional_class,
                    codon_change,
                    amino_acid_change,
                    amino_acid_length,
                    gene_name,
                    gene_biotype,
                    coding,
                    transcript,
                    exon,
                    genotype) = info.split('|')
                genotype_number = alleles[genotype]
            else:
                # Parse new ANN format
                try:
                    (allele,
                    annotation,
                    impact,
                    gene_name,
                    gene_id,
                    feature_type,
                    feature_id,
                    transcript_biotype,
                    exon,
                    hgvs_c,
                    hgvs_p,
                    cDNA_position_len,
                    cds_position_len,
                    protein_position_len,
                    distance_to_feature,
                    warnings_errors) = info.split('|')
                except:
                    (allele,
                    annotation,
                    impact,
                    gene_name,
                    gene_id,
                    feature_type,
                    feature_id,
                    transcript_biotype,
                    exon,
                    hgvs_c,
                    hgvs_p,
                    cDNA_position_len,
                    cds_position_len,
                    protein_position_len,
                    distance_to_feature) = info.split('|')
                # ANN cancer alleles may have format eg. 'A-C'
                if '-' in allele:
                    allele = allele.split('-')[0]
                genotype_number = alleles[allele]
            
            # Check if the mutant has the allele for which this effect applies
            if mutant_gt['AD'][genotype_number] == 0:
                continue
            
            # Calculate the ratio of depth at causative allele to total depth
            # for mutant
            mutant_allele_depth_ratio = self.allele_depth_ratio(mutant_gt,
                                                            genotype_number)

            # Check if this impact is equal to or above the minimum impact
            if (self.impacts_rank[impact]
                < self.impacts_rank[self.minimum_impact]):
                continue

            # Check if this impact is equal to or above the current highest
            if (d['highest_impact'] is None or
                self.impacts_rank[impact]
                > self.impacts_rank[d['highest_impact']]):
                # It is, update
                d['highest_impact'] = impact
                d['highest_impact_gene'] = gene_name
                d['highest_impact_effect'] = effect
                d['highest_impact_mutant_allele_depth_ratio'] = mutant_allele_depth_ratio
                    
            if self.snpeff_info_key == 'EFF':
                d['effects'].append(
                    {
                        'impact': impact,
                        'func_class': functional_class,
                        'codon_change': codon_change,
                        'aa_change': amino_acid_change,
                        'aa_length': amino_acid_length,
                        'gene_name': gene_name,
                        'gene_biotype': gene_biotype,
                        'coding': coding,
                        'transcript': transcript,
                        'exon': exon,
                        'mutant_allele_depth_ratio': mutant_allele_depth_ratio
                    }
                )
            else:
                d['effects'].append(
                    {
                        'impact': impact,
                        'annotation': annotation,
                        'codon_change': codon_change,
                        'aa_change': amino_acid_change,
                        'aa_length': amino_acid_length,
                        'gene_name': gene_name,
                        'transcript_biotype': transcript_biotype,
                        'exon': exon,
                        'HGVS.c': hgvs_c,
                        'HGVS.p': hgvs_p,
                        'cDNA_position / cDNA_len': cDNA_position_len,
                        'CDS_position / CDS_len': cds_position_len,
                        'Protein_position / Protein_len': protein_position_len,
                        'mutant_allele_depth_ratio': mutant_allele_depth_ratio
                    }
                )

        return d
        
    def allele_depth_ratio(self, gt, genotype_number):
        """Calculate the ratio of the allele at genotype number to the total
        depth
        
        Arguments:
            gt (vcf.model._Call): a sample genotype from VCFRecord (must be
                GATK-style, ie. allele depths as list with key 'AD'
            genotype_number: the index of the allele that causes the effect
        
        Return:
            ratio (float)
        """
        total_depth = sum(gt['AD'])
        # Get depth of allele that's causing the snpEff impact
        allele_depth = gt['AD'][genotype_number]
        ratio = float(allele_depth) / float(total_depth)
        return ratio

    def reserved_keys(self):
        return [self.snpeff_info_key]

class AlleleDepthsParser(object):
    """Parse genotype for each sample in VCFRecord into allele depths string"""

    name = 'allele_depths'

    @classmethod
    def customize_parser(self, parser):
        parser.add_argument(
            "-adso",
            "--allele-depths-sample-order",
            action="append",
            help="order in which to return allele depths"
        )

    def __init__(self, args):
        self.sample_order = args.allele_depths_sample_order
    
    def __call__(self, record):
        """Get allele depths as list"""

        ad_list = []
        
        # Add the header
        # Make alleles string
        alleles_string = ",".join([str(allele) for allele in record.alleles])
        ad_list.append("Sample: ({0})".format(alleles_string))

        # Add a string for each sample
        samples = self.sample_order or [s.sample for s in record.samples]
        for sample in samples:
            gt = record.genotype(sample)
            ad_string = self.allele_depths_string(gt)
            ad_list.append(ad_string)

        return ad_list

    def allele_depths_string(self, gt):
        depths = ",".join([str(ad) for ad in gt['AD']]) if gt.called else "-"
        return "{0}: {1}".format(gt.sample, depths)

    def reserved_keys(self):
        return []

class ScoresParser(object):
    """Extract specific INFO fields into a 'scores' list"""

    name = 'scores'

    @classmethod
    def customize_parser(self, parser):
        parser.add_argument(
            "--score-keys",
            nargs="+",
            required=True,
            help="score keys"
        )

    def __init__(self, args):
        self.score_keys = args.score_keys

    def __call__(self, record):
        """Extract wanted scores from record.INFO"""
        scores = []
        for score_key in self.score_keys:
            if score_key in record.INFO:
                scores.append([score_key, float(record.INFO[score_key])])
            else:
                scores.append([score_key, '-'])
        return scores

    def reserved_keys(self):
        return self.score_keys
