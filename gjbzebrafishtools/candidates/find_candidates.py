#!/usr/bin/env python

from __future__ import print_function

import sys
import argparse
import json

import pkg_resources
from collections import Counter

import vcf
from vcf.parser import _Filter

def create_filt_parser(name):
    parser = argparse.ArgumentParser(
        description='Parser for %s' % name,
        add_help=False
    )
    parser.add_argument('rest',
        nargs=argparse.REMAINDER,
        help=argparse.SUPPRESS
    )

    return parser

def create_core_parser():
    # we have to use custom formatted usage, because of the
    # multi-stage argument parsing (otherwise the filter arguments
    # are grouped together with the other optionals)
    parser = argparse.ArgumentParser(
        description='Filter a VCF file',
        add_help=False,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        usage="""%(prog)s [-h] [--no-short-circuit] [--no-filtered]
          [--output OUTPUT]
          input filter [filter_args] [filter [filter_args]] ...
        """
    )
    parser.add_argument(
        '-h',
        '--help',
        action='store_true',
        help='Show this help message and exit.'
    )
    parser.add_argument(
        'input',
        metavar='input',
        type=argparse.FileType('rb'),
        nargs='?',
        default=None,
        help='File to process (use - for STDIN)'
    )
    parser.add_argument(
        '--no-short-circuit',
        action='store_true',
        help='Do not stop filter processing on a site if any filter is triggered'
    )
    parser.add_argument(
        '--output',
        action='store',
#        default=sys.stdout,
        help='Filename to output [STDOUT]'
    )
    parser.add_argument(
        '--no-filtered',
        action='store_true',
        help='Output only sites passing the filters'
    )
    parser.add_argument(
        'rest',
        nargs=argparse.REMAINDER,
        help=argparse.SUPPRESS
    )

    return parser

# argument parsing strategy from `PyVCF <https://pyvcf.readthedocs.io/en/latest/>`

def main():
    # dynamically build the list of available filters
    filters = {}

    # parse command line args
    # (mainly because of local_script)
    parser = create_core_parser()
    (args, unknown_args) = parser.parse_known_args()

    # add filter to dictionary, extend help message
    # with help/arguments of each filter
    def addfilt(filt):
        filters[filt.name] = filt
        arg_group = parser.add_argument_group(filt.name, filt.__doc__)
        filt.customize_parser(arg_group)

    # look for global extensions
    for p in pkg_resources.iter_entry_points('gjb_vcf_filters'):
        filt = p.load()
        addfilt(filt)

    # go through the filters on the command line
    # one by one, trying to consume only the declared arguments
    used_filters = []
    while len(args.rest):
        filter_name = args.rest.pop(0)
        if filter_name not in filters:
            sys.exit("%s is not a known filter (%s)" %
                (filter_name, str(filters.keys())))

        # create a parser only for arguments of current filter
        filt_parser = create_filt_parser(filter_name)
        filters[filter_name].customize_parser(filt_parser)
        (known_filt_args, unknown_filt_args) = filt_parser.parse_known_args(
            args.rest)
        if len(unknown_filt_args):
            sys.exit("%s has no arguments like %s" %
                (filter_name, unknown_filt_args))

        used_filters.append((filter_name, known_filt_args))
        args.rest = known_filt_args.rest

    # print help using the 'help' parser, so it includes
    # all possible filters and arguments
    if args.help or len(used_filters) == 0 or args.input == None:
        parser.print_help()
        parser.exit()

    # Open input VCF
    inp = vcf.Reader(args.input)

    # build filter chain
    chain = []
    for (name, filter_args) in used_filters:
        f = filters[name](filter_args)
        chain.append(f)
        # Get the metadata string
        f_meta = f.header_metadata()
        inp.metadata[f_meta[0]] = f_meta[1]
        # Get the info strings
        try:
            f_infos = f.header_infos()
        except:
            f_infos = None
        if f_infos is not None:
            for f_info in f_infos:
                inp.infos[f_info[0]] = f_info[1]
        # add a filter record to the output
        short_doc = f.__doc__ or ''
        short_doc = short_doc.split('\n')[0].lstrip()
        inp.filters[f.filter_name()] = _Filter(f.filter_name(), short_doc)

    # output must be created after all the filter records have been added
    output = (vcf.Writer(open(args.output, 'w'), inp) if args.output
        else vcf.Writer(sys.stdout, inp))

    # apply filters
    short_circuit = not args.no_short_circuit
    drop_filtered = args.no_filtered
    dropped = Counter()
    n_records_seen = 0
    n_records_dropped = 0
    
    for record in inp:
        n_records_seen += 1
        
        output_record = True
        
        for filt in chain:
            result = filt(record)
            
            if result == None:
                continue

            dropped[result] += 1

            # save some work by skipping the rest of the code
            if drop_filtered:
                output_record = False
                break

            record.add_filter(result)
            
            if short_circuit:
                break

        if output_record:
            # use PASS only if other filter names appear in the FILTER column
            #FIXME: is this good idea?
            if record.FILTER is None and not drop_filtered:
                record.FILTER = 'PASS'
            output.write_record(record)
        else:
            n_records_dropped += 1
            if not n_records_seen % 50000:
                print("{0} records seen; {1} records dropped".format(
                    n_records_seen, n_records_dropped),
                    file=sys.stderr)

    dropped_counts = sorted(
        [(k, v) for k, v in dropped.iteritems()],
        key=lambda d: d[1],
        reverse=True
    )
    for dc in dropped_counts:
        print("{0}\t{1}".format(dc[0], dc[1]), file=sys.stderr)

if __name__ == '__main__': main()
