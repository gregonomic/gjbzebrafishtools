import vcf.filters
import re
from vcf.parser import _Info as VcfInfo
#from vcf.parser import field_counts as vcf_field_counts

class CandidateSnp(vcf.filters.Base):
    """ Identify candidate SNPs in forward mutagenic screen.
    
    VCF schould contain genotypes of pools of mutants vs siblings or
    mutants vs either pools or individuals from mutagenic and mapping
    backgrounds.
    """
    
    name = 'candidate_snp'
    
    @classmethod
    def customize_parser(self, parser):
        parser.add_argument(
            "-csm",
            "--candidate-snp-mutant-id",
            required="true",
            action="store",
            help="id of mutant (ie. affected) sample"
        )
        parser.add_argument(
            "-csu",
            "--candidate-snp-unaffected-sibling-id",
            action="store",
            help="id of control (ie. unaffected sibling) sample"
        )
        parser.add_argument(
            "-csx",
            "--candidate-snp-exclude-ids",
            action='append',
            default=[],
            help="id(s) of sample(s) to ignore"
        )
        parser.add_argument(
            "-csk",
            "--candidate-snp-ignore-known",
            action="store_true",
            help="ignore known (ie. previously annotated) SNPs"
        )
        parser.add_argument(
            "-css",
            "--candidate-snp-strict-only",
            action="store_true",
            help="write only strict SNPs"
        )
    
    @classmethod
    def header_metadata(self):
        return ('CandidateSnpVersion', ['0.0.1dev'])
    
    @classmethod
    def header_infos(self):
        return [
            (
                'STRICT_CANDIDATE',
                VcfInfo(
                    'STRICT_CANDIDATE',
                    num=0,
                    type='Flag',
                    desc='Does this candidate satisfy the \'strict\' criteria,\
 ie. its majority allele is not present in any of the reference samples. \
 Option: True, False',
                    source=None,
                    version=None
                )
            ),
            (
                'MUTANT_IS_HOM',
                VcfInfo(
                    'MUTANT_IS_HOM',
                    num=0,
                    type='Flag',
                    desc='Mutant is homozygous. Option: True, False',
                    source=None,
                    version=None
                )
            ),
            (
                'OTHERS_HAVE_MUT',
                VcfInfo(
                    'OTHERS_HAVE_MUT',
                    num=0,
                    type='Flag',
                    desc='One (or more) of the \'other\' samples has the \
majority mutant allele. Option: True, False',
                    source=None,
                    version=None
                )
            )
        ]

    def __init__(self, args):
#        self.threshold = "strict"
        self.mutant_id = args.candidate_snp_mutant_id
        self.unaffected_sibling_id = args.candidate_snp_unaffected_sibling_id
        self.exclude_ids = args.candidate_snp_exclude_ids
        self.ignore_known = args.candidate_snp_ignore_known
        self.strict = args.candidate_snp_strict_only
        self.threshold = "strict" if self.strict else "relaxed"

    def __call__(self, record):

        if self.ignore_known and (record.ID is not None):
            return  "KNOWN_SNP"
        
        # Get ref and alt bases; ALT is a list
        bases = [record.REF] + record.ALT
        
        # Get mutant_gt
        mutant_gt  = record.genotype(self.mutant_id)
        if not mutant_gt.called:
            return "NOT_CANDIDATE"
        
        mutant_is_hom = self.has_single_allele(mutant_gt)
        if mutant_is_hom:
            record.add_info('MUTANT_IS_HOM')

        # Get sibling_gt
        try:
            sibling_gt = record.genotype(self.unaffected_sibling_id)
        except:
            sibling_gt = None

        # Get other_gts
        other_gts = [gt for gt in record.samples
            if ((gt.sample != self.mutant_id)
             and (gt.sample != self.unaffected_sibling_id)
             and (gt.sample not in self.exclude_ids))]
             
        called_other_gts = [gt for gt in other_gts if gt.called]
        
        # Check if 'others' share any alleles with mutant
        others_have_mut_allele = self.others_have_mutant_allele(
            mutant_gt, called_other_gts)
        if others_have_mut_allele:
            record.add_info('OTHERS_HAVE_MUT')

        # set is_candidate and is_strict_candidate attributes
        is_candidate = self.is_candidate_snp(mutant_gt,
                                             sibling_gt,
                                             called_other_gts)

        if not is_candidate:
            return "NOT_CANDIDATE"

        is_strict_candidate = self.is_strict_candidate_snp(record,
                                                           other_gts)
        if is_strict_candidate:
            record.add_info('STRICT_CANDIDATE')

        # It's a candidate
        if self.strict and not is_strict_candidate:
            # Only strict candidates wanted
            return "NOT_STRICT_CANDIDATE"
        else:
            # Either 'strict only' not specified and this is a candidate,
            # or 'strict only' specified and this is a strict candidate
            return

    def is_candidate_snp(self, mutant_gt, sibling_gt, called_other_gts):
        """Determine whether this is a candidate SNP.
        
        Candidate criteria:
            1. The mutant must be covered to a depth of >= 1 read
            2. At least one of the unaffected sibling or other samples must be \
covered to a depth of >= 1 read
            3. If the mutant and sibling have a single allele, it must not be \
the same in both
            4. The mutant must have a majority allele that is:
                a. not the majority allele in any of the 'other' samples'''
        """
        
        # Check that mutant is called, ie. covered by at least one (good
        # quality) read
        if not mutant_gt.called:
            return False
        
        # Check at least one of the unaffected sibling and other gts is covered
        if sibling_gt is not None:
            if (not sibling_gt.called) and (len(called_other_gts) == 0):
                return False
        else:
            if len(called_other_gts) == 0:
                return False
        
        # Get index for mutant most abundant allele
        mut_most_abund_allele_idx = self.most_abundant_allele_index(
                mutant_gt)
#        if mutant_most_abundant_allele_index is None:
#            return False

        # If the sibling is called, and is homozygous, make sure its allele
        # is not the same as the mutant most abundant allele
        if sibling_gt is not None and sibling_gt.called:
            sibling_is_hom = self.has_single_allele(sibling_gt)
            if sibling_is_hom:
                sib_most_abund_allele_idx = self.most_abundant_allele_index(
                        sibling_gt)
                if sib_most_abund_allele_idx == mut_most_abund_allele_idx:
                    #...and they're the same allele
                    return False
        
        # Check that the majority allele in the mutant is not the majority
        # allele any of the 'other' samples
        for other_gt in called_other_gts:
            other_most_abund_allele_idx = self.most_abundant_allele_index(
                    other_gt)
            if other_most_abund_allele_idx == mut_most_abund_allele_idx:
                return False
        
        # Default is to return True
        return True
    
    def is_strict_candidate_snp(self, record, other_gts):
        """Determine whether this is a strict candidate SNP.
        
        Strict criteria:
            1. Mutant is homozygous
            2. All of the 'other' samples are called
            3. Mutant allele is not present in any of the 'other' samples
        """
        # Does mutant have only one allele?
        if 'MUTANT_IS_HOM' not in record.INFO:
#        if not record.INFO['MUTANT_IS_HOM']:
            return False
        
        # Are all the other samples called?
        for gt in other_gts:
            if not gt.called:
                return False
        
        # Do any of the other samples have the 'mutant' allele
        if 'OTHERS_HAVE_MUT' in record.INFO:
#        if record.INFO['OTHERS_HAVE_MUT']:
            return False
        return True
    
    def has_single_allele(self, gt):
        """gt is called, and only has one allele with depth > 0"""
        if not gt.called:
            return False
        # Is called, check there's only 1 covered allele
        return len([ad for ad in gt['AD'] if ad > 0]) == 1
    
    def most_abundant_allele(self, gt):
        '''Get the index for the most abundant allele'''
        return gt.alleles[self.most_abundant_allele_index(gt)]

    def most_abundant_allele_index(self, gt):
        '''Get the index for the most abundant allele'''
        if not gt.called:
            return None
        return sorted([(i, d) for i, d in enumerate(gt['AD'])],
                        key=lambda i_d: i_d[1],
                        reverse=True)[0][0]

    def others_have_mutant_allele(self, mutant_gt, other_gts):
        mutant_most_abundant_allele_index = self.most_abundant_allele_index(
                                                mutant_gt)
        for other_gt in other_gts:
            if other_gt['AD'][mutant_most_abundant_allele_index] > 0:
                # other_gt has depth > 0 for mutant_most_abundant_allele
                return True
        return False

class SnpEffFilter(vcf.filters.Base):
    """ Filter SNPs based on SnpEff annotation """

    name = 'snpeff'
    # Make dict for ranking impacts
    impacts_rank = {'MODIFIER': 0, 'LOW': 1, 'MODERATE': 2, 'HIGH': 3}
     # Initialise regular expression for parsing EFF strings
    re_pattern = re.compile("(.*)\((.*)\)")

    @classmethod
    def customize_parser(self, parser):
        parser.add_argument(
            "-sem",
            "--snpeff-mutant-id",
            action="store",
            required="true",
            help="id of mutant (ie. affected) sample"
        )
        parser.add_argument(
            "-see",
            "--snpeff-info-key",
            action="store",
            default='EFF',
            choices=['EFF', 'ANN'],
            help="INFO key that identifies snpEff annotation, either 'EFF' or \
'ANN'; default='EFF'"
        )
        effect_group = parser.add_mutually_exclusive_group(required=True)
        effect_group.add_argument(
            "-semi",
            "--snpeff-minimum-impact",
            action="store",
            choices=["HIGH", "MODERATE", "LOW", "MODIFIER"],
            help="minimum impact(s), options=['MODIFIER', 'LOW', 'MODERATE', \
'HIGH']; "
        )
        effect_group.add_argument(
            "-sei",
            "--snpeff-impact",
            action="append",
            choices=["HIGH", "MODERATE", "LOW", "MODIFIER"],
            help="required impact(s), options=['MODIFIER', 'LOW', 'MODERATE', \
'HIGH']; use -i <impact> for each required impact"
        )

    @classmethod
    def header_metadata(self):
        return ('SnpEffFilterVersion', ['0.0.1dev'])
    
    @classmethod
    def header_infos(self):
        return None

    def __init__(self, args):
        self.mutant_id = args.snpeff_mutant_id
        self.snpeff_info_key = args.snpeff_info_key
        self.minimum_impact = args.snpeff_minimum_impact
        self.impacts = args.snpeff_impact
        self.threshold = None

    def __call__(self, record):
        self.threshold = None

        # Get SnpEff annotation
        snpeff_result = (record.INFO[self.snpeff_info_key]
            if self.snpeff_info_key in record.INFO else None)
        if snpeff_result is None:
            return "NO_SNPEFF_INFO"
        mutant_gt  = record.genotype(self.mutant_id)
        if not mutant_gt.called:
            return "MUTANT_NOT_COVERED"
        alleles = {str(a):i for i,a in enumerate(record.alleles)}
        
        # Now parse all SnpEff results, updating impact and adding them to
        # effects list where appropriate
        seen_impacts = set()
        for result in snpeff_result:
            e_i = self.re_pattern.search(result)
            effect = e_i.group(1)
            info   = e_i.group(2)
            if self.snpeff_info_key == 'EFF':
                try:
                    (impact,
                    functional_class,
                    codon_change,
                    amino_acid_change,
                    amino_acid_length,
                    gene_name,
                    gene_biotype,
                    coding,
                    transcript,
                    exon,
                    genotype_number,
                    warnings_errors) = info.split('|')
                except:
                    (impact,
                    functional_class,
                    codon_change,
                    amino_acid_change,
                    amino_acid_length,
                    gene_name,
                    gene_biotype,
                    coding,
                    transcript,
                    exon,
                    genotype_number) = info.split('|')
            else:
                try:
                    (allele,
                    annotation,
                    impact,
                    gene_name,
                    gene_id,
                    feature_type,
                    feature_id,
                    transcript_biotype,
                    exon,
                    hgvs_c,
                    hgvs_p,
                    cDNA_position_len,
                    cds_position_len,
                    protein_position_len,
                    distance_to_feature,
                    warnings_errors) = info.split('|')
                except:
                    (allele,
                    annotation,
                    impact,
                    gene_name,
                    gene_id,
                    feature_type,
                    feature_id,
                    transcript_biotype,
                    exon,
                    hgvs_c,
                    hgvs_p,
                    cDNA_position_len,
                    cds_position_len,
                    protein_position_len,
                    distance_to_feature) = info.split('|')
                if '-' in allele:
                    allele = allele.split('-')[0]
                genotype_number = alleles[allele]
            
            # Check if the mutant has the allele for which this effect applies
            if mutant_gt['AD'][alleles[genotype_number]] == 0:
                continue

            # Compare this impact to minimum impact/wanted impacts
            if not self.wanted_impact(impact):
                continue
                
            seen_impacts.add(impact)

        # Finally, check if any of the seen impacts were wanted
        if len(seen_impacts) == 0:
            return "SNPEFF_IMPACT_TOO_LOW"

    def wanted_impact(self, impact):
        if self.minimum_impact:
            # minimum impact specified
            if (self.impacts_rank[impact]
                    >= self.impacts_rank[self.minimum_impact]):
                return True
        else:
            # selected impacts specified
            if impact in self.impacts:
                return True
        return False

