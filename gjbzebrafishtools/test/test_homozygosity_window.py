import unittest

from homozygosity.homozygosity_window import HomozygosityWindow, HomozygosityWindowNbps, HomozygosityWindowNsnps
from homozygosity.contig_coordinates import VCFContig, VCFContig, ContigSegment

class TestHomozygosityWindow(unittest.TestCase):

    def test_step_size_gt_window_size(self):
        chr = 'chr1'
        window_start = 1
        segment = ContigSegment(1, 508053)
        window_size = 10000
        step_size = 20000
        with self.assertRaises(ValueError):
            window = HomozygosityWindow.factory(
                chr,
                window_start,
                segment,
                window_size,
                step_size
            )

    def test_end_inside_segment_end(self):
        chr = 'chr1'
        window_start = 1
        segment = ContigSegment(1, 508053)
        window_size = 10000
        step_size = 1000
        window = HomozygosityWindow.factory(
            chr,
            window_start,
            segment,
            window_size,
            step_size
        )
        self.assertEqual(window.end, 10000)

    def test_end_outside_segment_end(self):
        chr = 'chr1'
        window_start = 508153
        segment = ContigSegment(508153, 509072)
        window_size = 10000
        step_size = 1000
        window = HomozygosityWindow.factory(
            chr,
            window_start,
            segment,
            window_size,
            step_size
        )
        self.assertEqual(window.end, segment.end)

if __name__ == '__main__':
    unittest.main()
