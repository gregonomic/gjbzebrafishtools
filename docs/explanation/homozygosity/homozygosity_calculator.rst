======================
Homozygosity detection
======================

Detection of regions of homozygosity in mutant pools is performed with
homozygosity_calculator.py or foshzl (*Fo*rward *S*creen *H*omo*z*ygosity
*L*ocator; pronounced fo-shizzle).

Detection is performed using VCF data as input. Two primary use cases are
anticipated:
- comparison of a single pool of affected mutants vs a single pool of
unaffected siblings
- comparison of a single pool of affected mutants vs the sequenced genomes of
the two parent strains:
-- the mutagenised strain, also referred to as the ENU strain in this package
-- the strain used for outcrossing, also referred to as the mapping strain

Homozygosity calculations are performed on sliding 'windows' of the genome. The
key properties of sliding windows are the window size (eg. distance in bps from
the start of the window to the end of the window) and the step size (eg. the
distance in bps from the start of one window to the start of the next window).
Windows can be non-overlapping (ie. when the step size is equal to the window
size) or overlapping (ie. step size is less than the window size). It is assumed
that the user will not request a step size greater than the window size, in
which case there will be gaps between the end of one window and the start of
the next.

The default method for measuring window size is in number of base-pairs.
However, it is sometimes useful (eg. when SNP density is low) to partition
based on number of SNPs; to do this, use the -nsnps/--use-nsnps-windows option.

Depending on the amount of variability in the genomes involved, some testing of
useful window sizes may be required. A useful starting point is a window size
of 10000 (for windows defined by number of base-pairs) or 200 (for windows
defined by number of SNPs).

Homozygosity windows have a 'counts' property (an instance of the
collections.Counter class), which is used to count properties of interest in
each window, which are subsequently used to calculate the score for the window.
The properties for each record are
