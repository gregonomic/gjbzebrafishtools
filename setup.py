try:
    import setuptools
    from setuptools import setup, find_packages
except ImportError:
    from distutils.core import setup, find_packages

with open("README.rst", "r") as fh:
    long_description = fh.read()

packages = find_packages()

config = {
    'description': 'Python tools for identifying regions of homozygosity and \
candidate mutations in whole-genome sequencing results of forward-genetic \
screens',
    'author': 'Greg Baillie',
    'url': 'https://bitbucket.org/gregonomic/gjbzebrafishtools/src/master/',
    'download_url': 'https://bitbucket.org/gregonomic/gjbzebrafishtools/get/master.tar.gz',
    'author_email': 'g.baillie@uq.edu.au',
    'version': '0.0.1dev',
    'long_description': long_description,
    'long_description_content_type': "text/x-rst",
    'install_requires': ['pyvcf'],
    'packages': find_packages(),
    'py_modules': ['gjbzebrafishtools.homozygosity',
                   'gjbzebrafishtools.candidates',
                   'gjbzebrafishtools.vcf_to_json'],
    'name': 'izl',
    'entry_points': {
        'homozygosity.scorers' : [
            'mut_sib = gjbzebrafishtools.homozygosity.homozygosity_scorer:MutantSibScorer',
            'mut_ref_imb = gjbzebrafishtools.homozygosity.homozygosity_scorer:MutantRefIMBScorer',
            'mut_ref_imb2 = gjbzebrafishtools.homozygosity.homozygosity_scorer:MutantRefIMB2Scorer',
            'mut_ref_henke = gjbzebrafishtools.homozygosity.homozygosity_scorer:MutantRefHenkeScorer',
        ],
        'gjb_vcf_filters': [
            'candidate_snp = gjbzebrafishtools.candidates.gjb_vcf_filters:CandidateSnp',
            'snpeff = gjbzebrafishtools.candidates.gjb_vcf_filters:SnpEffFilter'
        ],
        'vcf_json_parsers' : [
            'snpeff = gjbzebrafishtools.vcf_to_json.gjb_json_formatters:SnpEffParser',
            'allele_depth = gjbzebrafishtools.vcf_to_json.gjb_json_formatters:AlleleDepthsParser',
            'scores = gjbzebrafishtools.vcf_to_json.gjb_json_formatters:ScoresParser'
        ],
        'console_scripts': [
            'snzl = gjbzebrafishtools.candidates.find_candidates:main',
            'foshzl = gjbzebrafishtools.homozygosity.homozygosity_calculator:main',
            'vzl = gjbzebrafishtools.vcf_to_json.vcf_to_json:main',
        ]
    }
}

setup(**config)
